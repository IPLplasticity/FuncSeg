# plot BMA 2cnd level mean + ci
library(ggplot2)
library(igraph)
rm(list=ls())
tasks = c('inv','word','fb')
prefix <- "bma_"
prefix <- "BMA_"
prefix <- ""
get_from_to <- function(data) {
  ltpj_1_from <- c("Var1","Var2","Var3","Var4")
  ltpj_2_from <- c("Var5","Var6","Var7","Var8")
  rtpj_1_from <- c("Var9","Var10","Var11","Var12")
  rtpj_2_from <- c("Var13","Var14","Var15","Var16")

  ltpj_1_to <- c("Var1","Var5","Var9","Var13")
  ltpj_2_to <- c("Var2","Var6","Var10","Var14")
  rtpj_1_to <- c("Var3","Var7","Var11","Var15")
  rtpj_2_to <- c("Var4","Var8","Var12","Var16")

  data$from[data$var %in% ltpj_1_from] <- "l_TPJ-1"
  data$from[data$var %in% ltpj_2_from] <- "l_TPJ-2"
  data$from[data$var %in% rtpj_1_from] <- "r_TPJ-1"
  data$from[data$var %in% rtpj_2_from] <- "r_TPJ-2"

  data$to[data$var %in% ltpj_1_to] <- "l_TPJ-1"
  data$to[data$var %in% ltpj_2_to] <- "l_TPJ-2"
  data$to[data$var %in% rtpj_1_to] <- "r_TPJ-1"
  data$to[data$var %in% rtpj_2_to] <- "r_TPJ-2"
  return(data)
}
plot_graph <- function(data, main=NULL) {
  # data <- a_res

  d <- data[data$sig,]
  d <- na.omit(d)
  g <- graph_from_data_frame(d[,c("from","to")])
  col <- data$mean
  col[d$mean > 0] <- "green"
  col[d$mean < 0] <- "red"
  positions <- data.frame(name = c("l_TPJ-1", "l_TPJ-2", "r_TPJ-1", "r_TPJ-2"),
                          x = c(0,0,1,1),
                          y = c(1,0,1,0),
                          color = c("darkslategray1",
                                    "darkslategray2",
                                    "darkslategray1",
                                    "darkslategray2"),
                          stringsAsFactors = F)
  layout = c()
  colors = c()
  for (node in V(g)$name) {
    print(node)
    layout = c(layout, positions$x[positions$name == node], positions$y[positions$name == node])
    colors = c(colors, positions$color[positions$name == node])
  }
  layout <- matrix(layout, nrow = length(V(g)$name), byrow = T)
  V(g)$color <- colors
  V(g)$color <- colors
  E(g)$weight <- 100
  plot.igraph(g,layout    = layout,
              # edge.label  = paste0(d$mean,'\n(',round(d$p,2),')'),
              # edge.label  = paste0('\t\n\t',d$mean),
              edge.label  = d$label,
              edge.color  = col,
              vertex.size = 50,
              edge.arrow.size =.8,
              edge.width  = 3,
              vertex.label.font = 2,
              edge.label.font   = 2,
              edge.label.cex    = 1,
              vertex.label.cex  = 1.2,
              main        = main)
}

# read data ####
folder <- "dcm/dcm_cond/"
ep <- read.csv(paste0(folder,prefix,"Ep.csv"), col.names = c("mean"))
ep$mean <- round(ep$mean, 2)
ep$param <- 1:64
ep$var <- paste0('Var',1:16)

ep <- get_from_to(ep)
ep$type <- 'A'
ep$type[ep$param>16] <- 'B'
ep$task <- 'A'

for (i in 1:length(tasks)) {
  ep$task[(16+1+(i-1)*16):nrow(ep)] <- tasks[i]
}

cp <- read.csv(paste0(folder,prefix,"Cp.csv"), col.names = c("mean"))
cp$var<- paste0('Var',1:16)


ci <- qnorm(.9)
cp$mean <- sqrt(cp$mean) * ci
cp <- get_from_to(cp)
ep$sig <-abs(ep$mean)-abs(cp$mean) > 0


p <- ggplot(data=ep, aes(y=mean,x=param, color=type))+
  geom_bar(stat="identity")+
  geom_errorbar(aes(ymin=ep$mean-cp$mean,ymax=ep$mean+cp$mean))+
  theme_light()+
  ggtitle(substr(prefix,1,3))
ggsave(filename = paste0(prefix,'params.png'),p,width = 100,height = 70, units = 'mm')

ep$label <- ep$mean
ep$label[ep$from == ep$to] <- paste0(as.character(ep$mean[ep$from == ep$to]),'\n\n')
ep$label[ep$from != ep$to] <- paste0('\n\t',as.character(ep$mean[ep$from != ep$to]))

png(paste0(prefix,'a.png'),1000,700,pointsize = 16)
plot_graph(ep[ep$task == 'A',],   main=paste0(prefix,"A"))
dev.off()

png(paste0(prefix,'B_inv.png'),1000,700,pointsize = 16)
plot_graph(ep[ep$task == 'inv',], main=paste0(prefix,"inv"))
dev.off()

png(paste0(prefix,'B_fb.png'),1000,700,pointsize = 16)
plot_graph(ep[ep$task == 'fb',],  main=paste0(prefix,"fb"))
dev.off()

png(paste0(prefix,'B_word.png'),1000,700,pointsize = 16)
plot_graph(ep[ep$task == 'word',],main=paste0(prefix,"w"))
dev.off()
