basemat = '/data/pt_01994/probands/vp%02d/spm/uv_12s_fmriprep_acomp_deriv/DCM_clust_4nodes_3_cond_rest_thres%02d.mat';
fn_vois = {'/data/pt_01994/probands/vp%02d/spm/uv_12s_fmriprep_acomp_deriv/VOI_L_TPJ-1_rest_%d.mat';
    '/data/pt_01994/probands/vp%02d/spm/uv_12s_fmriprep_acomp_deriv/VOI_L_TPJ-2_rest_%d.mat';
    '/data/pt_01994/probands/vp%02d/spm/uv_12s_fmriprep_acomp_deriv/VOI_R_TPJ-1_rest_%d.mat';
    '/data/pt_01994/probands/vp%02d/spm/uv_12s_fmriprep_acomp_deriv/VOI_R_TPJ-2_rest_%d.mat'
    };

basemat = '/data/pt_01994/probands/vp%02d/spm/concat/DCM_clust_4nodes_3cond_rest_concat_alltasks.mat';

fn_vois = {'/data/pt_01994/probands/vp%02d/spm/concat/VOI_L_TPJ-1_rest_%01d.mat';
    '/data/pt_01994/probands/vp%02d/spm/concat/VOI_L_TPJ-2_rest_%01d.mat';
    '/data/pt_01994/probands/vp%02d/spm/concat/VOI_R_TPJ-1_rest_%01d.mat';
    '/data/pt_01994/probands/vp%02d/spm/concat/VOI_R_TPJ-2_rest_%01d.mat';
    };

res = table();
n_ses = 1;
for sub = [1:22]
    r_dcm_ses = [];
    r_voi_ses = cell(size(fn_vois,1),n_ses);
    n_voi_ses = cell(size(fn_vois,1),n_ses);
  
    fprintf('Subject %02d\n',sub);
    for ses = 1:n_ses
%         fprintf('\tSession %02d\n',ses);
        %% voi wise variance explained
        for voi_idx = 1:size(fn_vois,1)
            voi = load(sprintf(fn_vois{voi_idx}, sub, ses));
            var_expl = 100*voi.xY.s(1)/sum(voi.xY.s);
            r_voi_ses{voi_idx,ses} = var_expl;
            num_vox = length(voi.xY.s);
            n_voi_ses{voi_idx,ses} = num_vox ;
            
%             fprintf('\t\t voi %d: %2.2f var (%d voxel) \n',voi_idx, round(var_expl,2), num_vox);
        end
        
        
        
        %% DCM variance explained
        DCM = load(sprintf(basemat,sub));
        for i=1:DCM.DCM.M.l
            PSS(i)  = sum(DCM.DCM.y(:,i).^2);
            RSS(i)  = sum(DCM.DCM.R(:,i).^2);
            ud.R2.reg(i) = PSS(i)/(PSS(i) + RSS(i)); % coef of determination
        end
        ud.R2.tot =  sum(PSS)/sum(PSS+RSS);
        r_dcm_ses = [r_dcm_ses;ud.R2.tot];
%                 fprintf('\t\t DCM:   %02.2f\n',round(ud.R2.tot,2) );
        
        
       
    end
    for voi_idx = 1:size(fn_vois,1)
        fprintf('\t voi %d: %2.2f var (%d voxel) \n',voi_idx, round(mean([r_voi_ses{voi_idx,:}]),2), mean([n_voi_ses{voi_idx,:}]));
    end
    
    fprintf('\t mean DCM R2 %2.3f \n', mean(r_dcm_ses));
    disp('==================================================');
end