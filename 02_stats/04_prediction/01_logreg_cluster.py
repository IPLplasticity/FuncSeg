"""
OVR logistic regression to predict task membership from single trials.
Input data: cluster-wise attlased betas of conditions of interest

CV is done with a leave-one-subject out scheme


Files needed:
    * *betas_vpXX_spm.csv*: trial information this subject X
        columns:
         - beta_nr:   this is the corresponding beta****.nii image number
         - vp:        subject nr
         - run:       run 1-12
         - session:   session 1-3
         - ses_run:   1-4. relative to the corresponding session
         - runtime:   4 runs per sessions. this is the trial onset time relative to the corresp run.
         - sestime:   3 sessions per subject. this is trial onste time relative to the corresp session

         - task:      ATT, SEM, SOC
         - condition: val, inv, w, p, fb, tb (valid, invalid, word, pseudoword, false belief, true beflief
         - trial:     for SEM and SOC this is the identifier for the same trialover all subjects
         - cor:       correct respond (bool)
         - rt:        reaction time
         - left:      ATT: cue shown left? (bool)
         - tcue:      ATT: time lag after cue (250 or 350ms)
         - exp:       SOC: expected or unexpected (bool)

         - vpidx:     same as vp
         - vp.1:      subject string
         - dbnr:      subject MPI database nr
         - sex, age, lq, rs, fem: sex (m/f), age (int), left handedness score (int), reading span score (int), female
         - spm_desc:  beta description read from SPM.mat

    * *betaXXXX.nii*: subject/trial wise beta images from SPM in normalized space.

"""
from sklearn.model_selection import ShuffleSplit, LeaveOneGroupOut, KFold, cross_validate, StratifiedKFold
from sklearn.linear_model import LogisticRegressionCV, LogisticRegression
from sklearn.preprocessing import StandardScaler
from nilearn.input_data import NiftiLabelsMasker
from nilearn.plotting import plot_matrix
from sklearn.metrics import confusion_matrix
from sklearn.pipeline import Pipeline
from nilearn.signal import clean
import matplotlib.pyplot as plt
from nilearn import image
import pandas as pd
import numpy as np
import warnings
import yaml
import time
import os

np.set_printoptions(suppress=True)
np.set_printoptions(precision=3)
params = {
    # global parameters
    'subjects': list(range(1, 22)),
    'conditions_dict': {'ATT': ['inv'],
                        'SEM': ['w'],
                        'SOC': ['fb']},
    'conditions_noi': {'ATT': ['val'],
                       'SEM': ['p'],
                       'SOC': ['tb']},
    'random_state': 2,
    'subsample_data': 'condition',  # task, condition
    'verbose': 0,

    # directories
    'beta_subj_folder': "",
    'csv_folder': '',
    'prefix': "parcel",  # results files prefix
    'result_folder': '/data/pt_01994/results/mv/ovr_logreg_cluster/',
    'write_nii': True,  # write subject wise accuracy niftis

    # data parameters
    'atlas': True,
    'deconfound_method': 'signal.clean',  # own | signal.clean | None
    'only_cor': True,  # only correct answered trials to consider?
    'mask_fn': "",
    'rni': ['session', 'runtime', 'ses_run', 'sestime', 'run'],  # regressors of no interest. runtime, sestime, rt, ...
    'rni_cat': [True, False, True, False, True],  # # which rni to be computed as one-hot
    'target_var': 'condition',  # what to classify? task/condition/cor
    'shuffle_y': 0, # if 1, y is shuffled to test for bugs

    # regression parameters
    'reg_model': 'lr',  # lr (logistic regression), lr_cv (with cross validation),
    'reg_penality': 'none',  # l2 or l1
    'reg_hyper_cv_folds': 4,  # if lr_cv, how many folds for the hyperparameter estimation cv
    'reg_hyper_cv_cs': 10,  # number of Cs to test or float list of Cs to test
    'reg_hyper_c': 100000.,  # C for not cv-version of lr. not used if reg_penality=None

    # cross-validation scheme
    'cv_strategy': 'logo',  # kfold, shuffle, logo
    'fold_strat': 'vp',  # for logo: group definition. 'run', 'ses', None, vp
    'n_splits': 5,  # for shuffle and kfold
    'n_jobs': 25,
    'shuffle_kfold': True,
}
if params['shuffle_y']:
    params['prefix'] += "_shuffled({:0>3})".format(params['shuffle_y'])

atlas_fn = "cluster_combined.nii"

if not os.path.exists(params['result_folder']):
    os.makedirs(params['result_folder'])
masker = NiftiLabelsMasker(labels_img=atlas_fn, standardize=True, resampling_target='data')

# get all condition names
coi = np.array(list(params['conditions_dict'].values())).flatten().tolist()
if params['scale_by_coni']:
    coni = np.array(list(params['conditions_noi'].values())).flatten().tolist()

x = np.array([])
y = np.array([])
folds = None
csv = pd.DataFrame()
if params['fold_strat'] is not None:
    folds = []

confounds = None
confounds_cni = None
if params['rni']:
    confounds = pd.DataFrame()
    confounds_cni = pd.DataFrame()

for sub in params['subjects']:
    print("Working on subject {0:0>2}".format(sub))

    # read csv-data
    csv_sub = pd.read_csv(params['csv_folder'] + "betas_vp{0:0>2}_spm.csv".format(sub))
    csv_sub_noi = pd.read_csv(params['csv_folder'] + "betas_vp{0:0>2}_spm.csv".format(sub))

    # remove unwanted trials
    csv_sub = csv_sub.loc[csv_sub['condition'].isin(coi)]
    if params['scale_by_coni']:
        csv_sub_noi = csv_sub_noi.loc[csv_sub_noi['condition'].isin(coni)]

    if params['only_cor']:
        csv_sub = csv_sub.loc[csv_sub['cor']]
        if params['scale_by_coni']:
            csv_sub_noi = csv_sub_noi.loc[csv_sub_noi['cor']]

    # apply subsampling strategy
    if params['subsample_data'] is not None:
        subsample_by = params['subsample_data']
        if params['verbose']:
            print("Subsampling to min of {}".format(subsample_by))

        # subsample data to have each condition equal times
        # for logo cv we can assure that each condition is present equal times per fold

        # so group condition-wise trials per run
        min_occ = csv_sub.groupby(params['fold_strat'], observed=False)[params['target_var']].value_counts()


        def sample_fold_cond(x):
            """ Lil' helper to sample min number for given fold.

            Call with pd.groupby([params['fold_strat'], subsample_by]).apply(sample_fold_cond)
            """
            fold = x[params['fold_strat']].values[0]
            min_occ_fold = np.min(min_occ[fold])
            # drop this fold completeley if not all conditions are present
            if not set(coi) == set(min_occ[fold].index.to_list()):
                min_occ_fold = 0

            return (x.sample(min_occ_fold,
                                replace=False,
                                random_state=params['random_state']))


        if params['cv_strategy'] == 'logo':
            csv_sub = csv_sub.groupby([params['fold_strat'],
                                        subsample_by]).apply(sample_fold_cond).reset_index(drop=True)

            for cond in coi:
                cond_count = csv_sub.loc[
                    csv_sub['condition'] == cond].groupby(params['fold_strat'])['condition'].value_counts().values
                print("condition count {0: >3}: {1}".format(cond, cond_count))

        else:
            # if not logo cv, just take minimum condition per subject
            min_occ = csv_sub[subsample_by].value_counts().min()
            drops = pd.DataFrame()

            # how many trials to drop per condition?
            for cond in coi:
                dif = (csv_sub[subsample_by] == cond).sum() - min_occ
                drops = drops.append(csv_sub[csv_sub[subsample_by] == cond].sample(dif,
                                                                                    replace=False,
                                                                                    random_state=params[
                                                                                        'random_state']))
            csv_sub.drop(drops.index, inplace=True)

            assert (np.all(csv_sub[subsample_by].value_counts() == min_occ))

    # get beta filenames
    beta_fns = 'beta_' + csv_sub['beta_nr'].map('{:0>4}'.format) + ".nii"  # use 'beta_nr' column with 4 leading zeros
    beta_noi_fns = 'beta_' + csv_sub_noi['beta_nr'].map('{:0>4}'.format) + ".nii"
    beta_dir = params['beta_subj_folder'].format(sub)

    # load raw beta images and remove nans
    beta_img = image.load_img(beta_dir + beta_fns)
    beta_img = image.smooth_img(beta_img, fwhm=None)

    beta_noi_img = None
    if params['scale_by_coni']:
        beta_noi_img = image.load_img(beta_dir + beta_noi_fns)
        beta_noi_img = image.smooth_img(beta_noi_img, fwhm=None)

    if params['mask_fn']:
        mask = image.load_img(params['mask_fn'])
        mask = image.resample_to_img(mask, beta_img)
        beta_img.get_data()[mask.get_data() == 0] = 0

    x_sub_noi = None
    if params['scale_by_coni']:
        x_sub_noi = masker.fit_transform(beta_noi_img)

    if params['atlas']:
        x_sub = masker.fit_transform(beta_img)
    else:
        if params['mask_fn']:
            x_sub = beta_img.get_data()[mask.get_data() != 0]
        else:
            x_sub = beta_img.get_data()

    sub_confounds = sub_confounds_noi = None
    if params['rni']:
        confounds = confounds.append(csv_sub[params['rni']])
        confounds_noi = confounds_cni.append(csv_sub_noi[params['rni']])
        sub_confounds = csv_sub[params['rni']].values
        sub_confounds_noi = csv_sub_noi[params['rni']].values

    if params['deconfound_method'] == "signal.clean":
        if sub_confounds is not None:
            def apply_clean(x_tmp, conf, csv_data):
                for col in range(conf.shape[1]):
                    x_tmp = clean(x_tmp, sessions=csv_data['run'],
                                  detrend=False,
                                  standardize=False,
                                  confounds=conf[:, col])

                return x_tmp


            if not params['atlas']:
                x_sub = x_sub.T
            x_sub = apply_clean(x_sub, sub_confounds, csv_sub)
            if params['scale_by_coni']:
                x_sub_noi = apply_clean(x_sub_noi, sub_confounds_noi, csv_sub_noi)

    # remove confounds
    else:
        raise NotImplementedError("params['deconfound_method'] {} not known.".format(params['deconfound_method']))

    # get y for this subject
    y_sub = csv_sub[params['target_var']].values

    x_sub = clean(x_sub, sessions=csv_sub['run'],
                  detrend=False,
                  standardize=False,
                  confounds=sub_confounds)

    assert (y_sub.shape[0] == x_sub.shape[0])

    # append this subject's data to X and y
    x = np.vstack([x, x_sub]) if x.size else x_sub
    y = np.hstack([y, y_sub]) if y.size else y_sub
    csv = csv.append(csv_sub)
    if params['fold_strat'] is not None:
        folds += csv_sub[params['fold_strat']].values.tolist()

    print("Subject {0:0>2} done".format(sub))

print("target items:{}".format(["{}: {}".format(i, sum(y == i)) for i in np.unique(y)]))
"""logistic regression"""

if params['shuffle_y']:
    np.random.seed(params['shuffle_y'])
    np.random.shuffle(y)
    print("Shuffling y with seed {}".format(params['shuffle_y']))

# get cross validation scheme
if params['cv_strategy'] == 'shuffle':
    cv = ShuffleSplit(n_splits=params['n_splits'],
                      random_state=params['random_state'])

elif params['cv_strategy'] == 'logo':
    cv = LeaveOneGroupOut()

elif params['cv_strategy'] == 'kfold':
    cv = KFold(n_splits=params['n_splits'],
               shuffle=params['shuffle_kfold'],
               random_state=params['random_state'])

elif params['cv_strategy'] == '':
    cv = None

else:
    raise NotImplementedError("Unknown cv {}".format(params['cv_strategy']))

# get estimator
if params['reg_model'] == 'lr_cv':
    reg = LogisticRegressionCV(penalty=params['reg_penality'],
                               cv=params['reg_hyper_cv_folds'],
                               max_iter=1e4,
                               Cs=params['reg_hyper_cv_cs'],
                               multi_class='ovr',
                               random_state=params['random_state'],
                               verbose=params['verbose'],
                               solver='lbfgs')

elif params['reg_model'] == 'lr':
    reg = LogisticRegression(penalty=params['reg_penality'],
                             multi_class='ovr',
                             max_iter=1e4,
                             C=params['reg_hyper_c'],
                             random_state=params['random_state'],
                             verbose=params['verbose'],
                             solver='lbfgs')
else:
    raise NotImplementedError("params['reg_model'] = {} unknown.".format(params['reg_model']))

sc = StandardScaler()
if confounds is not None:
    confounds = confounds.values

pl = Pipeline([('transformer', sc), ('estimator', reg)])

print("Starting {} reg fit with {} crossvalidation".format(params['reg_model'],
                                                           params['cv_strategy']))
s = time.time()

cv_results = cross_validate(pl,
                            x,
                            y,
                            cv=cv,
                            groups=folds,
                            n_jobs=params['n_jobs'],
                            return_estimator=True,
                            return_train_score=True,
                            error_score='raise')
print("log reg fit done ({:.2f}s).".format(time.time() - s))

# create the confusion matrix
for i in range(len(cv_results['test_score'])):
    # get one cm per fold
    est = cv_results['estimator'][i].steps[1][1]
    if i == 0:
        cm = []
        folds = np.array(folds)
        classes = est.classes_

    fold_val = np.unique(folds)[i]
    y_test = y[folds == fold_val]
    x_test = x[folds == fold_val]
    assert np.all(est.classes_ == classes)
    cm.append(confusion_matrix(y_test, est.predict(x_test), labels=['fb', 'inv', 'w']))
cm = np.mean(np.array(cm), axis=0)
fig, ax = plt.subplots(nrows=1, ncols=1)
plot_matrix(cm,
            labels=est.classes_,
            title='Confusion matrix: One vs All', cmap='hot_r',
            figure=fig)

fig.savefig(params['result_folder'] +
            '{}.png'.format(params['prefix']))  # save the figure to file
plt.close(fig)  #

# get c values per class if crossvalidated version of lr was used
if params['reg_model'] == 'lr_cv':
    # print(f"c chosen automatically: {np.array([est.steps[1][1].C_ for est in cv_results['estimator']])}")
    c_chosen = np.mean(np.array([est.steps[1][1].C_ for est in cv_results['estimator']]), axis=0).tolist()
    params['res_best_c_per_class'] = c_chosen
    c_tested = cv_results['estimator'][0].steps[1][1].Cs_.tolist()
    params['res_c_tested'] = c_tested
else:
    c_tested = [params['reg_hyper_c']]

print("dec: {}".format(params['deconfound_method']))
print("rni: {}".format(params['rni']))
print("cat: {}".format(params['rni_cat']))

print("\t conditions: {}. scores testing/training: {:>2.10}/{:>2.10}.".format(coi,
                                                                              np.mean(cv_results['test_score']),
                                                                              np.mean(cv_results['train_score'])))

# get mean coefs per class over folds
coefs = np.array([est.steps[1][1].coef_ for est in cv_results['estimator']])
coefs = np.mean(coefs, axis=0)

# collect some scores
params['res_test_score_' + str(coi)] = float(np.mean(cv_results['test_score']))
params['res_train_score_' + str(coi)] = float(np.mean(cv_results['train_score']))
params['res_classes'] = cv_results['estimator'][0].classes_.tolist()

params['res_fb'] = float(cm[0, 0] / np.sum(cm[0, :]))
params['res_inv'] = float(cm[1, 1] / np.sum(cm[1, :]))
params['res_w'] = float(cm[2, 2] / np.sum(cm[2, :]))
print(cm)

# write params and results to .yaml file
with open(params['result_folder'] + '{}.yaml'.format(params['prefix']), 'w') as f:
    yaml.dump(params, f)

# write nifti
if len(cv_results['estimator'][0].classes_) > 2:
    for idx, cl in enumerate(cv_results['estimator'][0].classes_):
        if params['write_nii']:
            if params['atlas']:
                coef_img = masker.inverse_transform(coefs[idx, np.newaxis])
                coef_img.to_filename(params['result_folder'] +
                                     '{}_{}.nii.gz'.format(params['prefix'],
                                                           cl))
            else:
                data = coefs[idx]
                data_img = image.index_img(beta_img, 0)
                data_img.get_data()[mask.get_data() != 0] = data
                data_img.to_filename(params['result_folder'] +
                                     '{}_{}.nii.gz'.format(params['prefix'],
                                                           cl))
else:
    warnings.warn("Untested")
    if params['write_nii']:
        coef_img = masker.inverse_transform(coefs)
        coef_img.to_filename(params['result_folder'] +
                             '{}_{}.nii.gz'.format(params['prefix'],
                                                   coi))

print("=" * 64)
