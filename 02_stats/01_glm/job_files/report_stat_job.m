%-----------------------------------------------------------------------
% Job saved on 14-Sep-2018 10:09:52 by cfg_util (rev $Rev: 6942 $)
%-----------------------------------------------------------------------
matlabbatch{1}.spm.stats.results.spmmat = '<UNDEFINED>';
for i =1:18
    matlabbatch{1}.spm.stats.results.conspec(i).titlestr = '';
    matlabbatch{1}.spm.stats.results.conspec(i).contrasts = i;
    matlabbatch{1}.spm.stats.results.conspec(i).threshdesc = 'FWE';
    matlabbatch{1}.spm.stats.results.conspec(i).thresh = 0.05;
    matlabbatch{1}.spm.stats.results.conspec(i).extent = 0;
    matlabbatch{1}.spm.stats.results.conspec(i).conjunction = 1;
    matlabbatch{1}.spm.stats.results.conspec(i).mask.none = 1;
    matlabbatch{1}.spm.stats.results.units = 1;
end
matlabbatch{1}.spm.stats.results.export{1}.ps = true;
