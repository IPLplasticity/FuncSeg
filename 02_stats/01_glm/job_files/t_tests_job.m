matlabbatch{1}.spm.stats.con.spmmat = '<UNDEFINED>';

% 1  'ATT val';
% 2  'ATT inv';
% 3  'ATT cat';
% 4  'SEM word';
% 5  'SEM pseudoword';
% 6  'SOC tb e';
% 7  'SOC tb u';
% 8  'SOC fb e';
% 9  'SOC fb u';
% 10 'REST'

i = 1;
%% ATT
matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att val > Att inv';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 1  -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att > Rest';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 1  1 repelem(0,7) -2];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att val > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 1  repelem(0,8) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att inv > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 0  1 repelem(0,7) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att cat > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 0  0  1 repelem(0,6) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;


%% Sem
matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem w > Sem pw';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,3) 1 -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem > Rest';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,3) 1  1 repelem(0,4) -2];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem w > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,3) 1  repelem(0,5) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem pw > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,4) 1  repelem(0,4) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

%% SOC
% tb vs fb
matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'fb > tb';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5) -1 -1  1  1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb e > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5) 1 repelem(0,3) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb u > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,6) 1 repelem(0,2) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'fb e > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,7) 1 repelem(0,1) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'fb u > Rest';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,8) 1 repelem(0,1) -1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;
% 
% % subcontrasts
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb e > tb u';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5)  1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'fb e > fb u';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,7)  1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb e < fb e';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5) -1  0  1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb e > fb u';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5)  1  0  0 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb u > fb e';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,6)  1  0 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'tb > REST';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ repelem(0,5)  1  1  0 0 -2];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'fb > REST';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ repelem(0,7)  1  1  -2];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'SOC > rest';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,5)  1  1  1  1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;


% matlabbatch{1}.spm.stats.con.consess{2}.fcon.name = 'EOI4cond';
% matlabbatch{1}.spm.stats.con.consess{2}.fcon.weights = [eye(4)];
% matlabbatch{1}.spm.stats.con.consess{2}.fcon.sessrep = 'both';
% i = i+1;
%% Between Task
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att > Sem';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 1  1  1 -1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att < Sem';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [-1 -1 -1  1  1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem > Soc';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,3)  1  1 -1 -1 -1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Sem < Soc';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [repelem(0,3) -1 -1  1  1  1  1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Soc > Att';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [-1 -1 -1 repelem(0,2)  1  1  1  1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;
% 
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Soc < Att';
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 1  1  1 repelem(0,2) -1 -1 -1 -1];
% matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'both';
% i = i+1;

%% Between Task EOI
matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Soc fb e < Att inv';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ 0  1  0  0  0  0  0  -1 ];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Soc fb e < Sem w';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ repelem(0,3)  1  0  0  0  -1 ];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att inv < Sem w';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ repelem(0,1)  1  0  1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;

matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = 'Att inv < Sem w';
matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = [ repelem(0,1)  1  0  1];
matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = 'repl';
i = i+1;


matlabbatch{1}.spm.stats.con.delete = 1; % keine Ahnung was das macht....ist immer dabei ^^
