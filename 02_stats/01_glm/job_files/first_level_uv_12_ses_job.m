%-----------------------------------------------------------------------
% Job saved on 11-Sep-2018 12:40:31 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.stats.fmri_spec.dir = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 0.5;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 36;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 18; % was 2

matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(2).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(2).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(2).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(2).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(3).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(3).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(3).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(3).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(3).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(3).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(4).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(4).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(4).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(4).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(4).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(4).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(5).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(5).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(5).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(5).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(5).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(5).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(6).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(6).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(6).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(6).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(6).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(6).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(7).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(7).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(7).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(7).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(7).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(7).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(8).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(8).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(8).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(8).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(8).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(8).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(9).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(9).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(9).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(9).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(9).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(9).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(10).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(10).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(10).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(10).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(10).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(10).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(11).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(11).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(11).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(11).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(11).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(11).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.sess(12).scans = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(12).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(12).multi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(12).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(12).multi_reg = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess(12).hpf = 128;

matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
% matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = '<UNDEFINED>';
% matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
% matlabbatch{1}.spm.stats.fmri_spec.cvi = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'FAST';
% matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';