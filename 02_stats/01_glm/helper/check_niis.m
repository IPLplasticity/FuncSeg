function check_niis(vps, type)
% Display suject- & session-wise niftis for visual inspection.
basefolder = '/data/pt_01994/probands/';
vps = get_vp_names(vps);

switch type
    case 'raw'
        % norm, inv, epi
        filesuff = {'/nifti/2_cmrr_mbep2d_se_fast_norm.nii,1'
            '/nifti/3_cmrr_mbep2d_se_fast_invPol.nii,1'
            '/nifti/4_cmrr_mbep2d_bold_fast.nii,1'
            '/nifti/5_cmrr_mbep2d_se_fast_norm.nii,1'
            '/nifti/6_cmrr_mbep2d_se_fast_invPol.nii,1'
            '/nifti/7_cmrr_mbep2d_bold_fast.nii,1'
            '/nifti/8_cmrr_mbep2d_se_fast_norm.nii,1'
            '/nifti/9_cmrr_mbep2d_se_fast_invPol.nii,1'
            '/nifti/10_cmrr_mbep2d_bold_fast.nii,1'
            '/nifti/11_cmrr_mbep2d_se_fast_norm.nii,1'
            '/nifti/12_cmrr_mbep2d_se_fast_invPol.nii,1'
            '/nifti/13_cmrr_mbep2d_bold_fast.nii,1'};
    case 'vdm'
        % vdm, epi
        filesuff = {'/dc/vdm5_fpm_1_fieldmap.img';
            '/nifti/4_cmrr_mbep2d_bold_fast.nii,1';
            '/dc/vdm5_fpm_2_fieldmap.img';
            '/nifti/7_cmrr_mbep2d_bold_fast.nii,1';
            '/dc/vdm5_fpm_3_fieldmap.img';
            '/nifti/10_cmrr_mbep2d_bold_fast.nii,1';
            '/dc/vdm5_fpm_4_fieldmap.img';
            '/nifti/13_cmrr_mbep2d_bold_fast.nii,1';};
        
    case 'preproc_12'
        % wua, swua
        filesuff = {'_desp_cmrr_mbep2d_bold_fast.nii'};
        
    case 'meanfunc'
        filesuff = {'meanua1_desp_cmrr_mbep2d_bold_fast.nii'};
        
    case 'fmripreproc'
        
end

switch type
    case {'raw', 'vdm'}
        for vp = 1:size(vps,1)
            disp(vps(vp,:));
            t1 = ['/data/pt_01994/probands/' vps(vp,:) '/mr/t1/t1_MPRAGE_ADNI_32.nii'];
            for ses = 1:3
                disp(['  session: ' num2str(ses)]);
                files = cell(size(filesuff,1)+1, 1);
                for file = 1:size(filesuff,1)
                    files{file } = [basefolder vps(vp,:) '/mr/ses' num2str(ses) filesuff{file}];
                end
                files{end} =             t1;
                spm_check_registration(files{:})
                pause(6)
            end
        end
        
    case {'preproc_12'}
        for vp = 1:size(vps,1)
            disp(vps(vp,:));
            t1 = ['/data/pt_01994/probands/' vps(vp,:) '/mr/t1/t1_MPRAGE_ADNI_32.nii'];
            for ses = 1:3
                disp(['  session: ' num2str(ses)]);
                
                files = cell(0);
                for file = 1:4
                    files{end+1} = [basefolder vps(vp,:) '/spm/12ses_desp/wua' num2str((ses-1) * 4 + file) filesuff{1}];
                    files{end} = [files{end} ',' num2str(length(spm_data_hdr_read(files{end})))];
                    
                    files{end+1} = [basefolder vps(vp,:) '/spm/12ses_desp/wua' num2str((ses-1) * 4 + file) filesuff{1} ',1'];
                    
                end
                spm_check_registration(files{:});
            end
        end
    case {'meanfunc'}
        for vp = 1:size(vps,1)
           disp(['vp: ' vps(vp,:)]);
           files = ['/data/pt_01994/probands/' vps(vp,:) '/spm/12ses_desp/' filesuff{1}];
           spm_check_registration(files);
            pause(4);
        end
end