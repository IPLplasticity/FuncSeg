function show_contrasts(fn)
disp(['Contrasts for ' fn]);
SPM_n = load(fn, 'SPM');
SPM_n = SPM_n.SPM;
for i = 1:length(SPM_n.xCon)
    disp(SPM_n.xCon(i).name);
    for j = find(SPM_n.xCon(i).c == 1)'
%     SPM.xCon(i).c
    disp([' 1: ' num2str(j) ' ' SPM_n.Vbeta(j).descrip]);
    end
    for j = find(SPM_n.xCon(i).c == -1)'
%     SPM.xCon(i).c
    disp(['-1: ' num2str(j) ' '  SPM_n.Vbeta(j).descrip]);
    end
    disp(' ')
end
clear SPM_n
end