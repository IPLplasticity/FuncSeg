tasks = ['att';'sem';'soc'];

fn_t = '/data/pt_01994/results/spm/%s/SPM.mat'
contrast_idx = 1;

att_loc = [-54.0, -45.0, 38.15999937057495;
    -45.0, -36.0, 41.679999351501465;
    60.0, -39.0, 24.079999446868896;
    57.0, -48.0, 24.079999446868896];

sem_loc = [-51.0, -72.0, 27.59999942779541;
    -51.0, -30.0, 13.519999504089355;
    63.0, -24.0, 34.63999938964844;
    57.0, -39.0, 31.119999408721924];

soc_loc = [-48.0, -54.0, 31.119999408721924;
    -66.0, -27.0, 27.59999942779541;
    51.0, -36.0, 52.239999294281006;
    51.0, -60.0, 31.119999408721924];

locations = [-47.6595092 , -61.42177914,  31.81644112;
    -56.45901639, -31.99180328,  29.25420707;
    58.75123153, -29.43103448,  27.35724081;
    51.6741573 , -58.15569823,  31.26690149];

res = table();
val = []; se = []; ci = [];
task_res = []; location_res = []; clus = []; cond_res = [];
for task_idx = 1:3
    for loc_idx =1:4
        task = tasks(task_idx,:);
        if task == 'att'
            conds = ['in_vs_rest';'va_vs_rest'];
        elseif task == 'sem'
            conds = ['wo_vs_rest';'pw_vs_rest'];
        elseif task == 'soc'
            conds = ['fb_vs_rest';'tb_vs_rest'];
        else
            error('')
        end
        for cond_i = 1:2
            disp(task);
            fn = sprintf(fn_t,task, conds(cond_i,:));
            [v, s, c] = pull_estimates(fn,contrast_idx, locations(loc_idx,:));
            val = [val;v]; se = [se;s]; ci = [ci;c];
            task_res = [task_res;task]; cond_res = [cond_res;conds(cond_i,:)];
            location_res = [location_res;locations(loc_idx,:)];
            clus = [clus;loc_idx];
        end
    end
end

res = table(task_res, cond_res, clus,location_res, val, se, ci)
writetable(res, 'results/estimates_rest.csv');
