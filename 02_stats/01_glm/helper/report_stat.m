function report_stat(vps, subdir, uni)

vps = get_vp_names(vps);
parfor vp = 1:size(vps,1)
    if uni
        % List of open inputs
        % Results Report: Select SPM.mat - cfg_files
        % Results Report: Contrast(s) - cfg_entry
        % Results Report: Contrast(s) - cfg_entry
        nrun = 1; % enter the number of runs here
        jobfile = {'stats/frst_lvl/job_files/report_stat_job.m'};
        jobs = repmat(jobfile, 1, nrun);
        inputs = cell(3, nrun);
        for crun = 1:nrun
            inputs{1, crun} = {['/data/pt_01994/probands/' vps(vp,:) '/spm/' subdir '/SPM.mat']}; % Results Report: Select SPM.mat - cfg_files
        end
        spm('defaults', 'FMRI');
        spm_jobman('run', jobs, inputs{:});
    else
        error("Not implemented");
    end
end
end