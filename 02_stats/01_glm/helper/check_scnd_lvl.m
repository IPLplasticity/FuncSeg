% CHECK_SCDN_LVL shows all SPM.mat dependencies, up to the 1st level beta
% images.
basefolder = ''

subfolders = {'/att/inv_vs_val/SPM.mat';
    '/att/val_vs_inv/SPM.mat';
    '/sem/pw_vs_w/SPM.mat';
    '/sem/w_vs_pw/SPM.mat';
    '/soc/e_vs_u/SPM.mat';
    '/soc/fb_vs_tb/SPM.mat';
    '/soc/tb_vs_fb/SPM.mat';
    '/soc/u_vs_e/SPM.mat'};

fns = cell(numel(subfolders),1);
for i = 1:numel(subfolders)
   fns{i} = [basefolder subfolders{i}];
end
logfilename = '';
fid = fopen(logfilename,'w');
subj_spms = {};
fclose(fid);

for fn_i = 1:size(fns,1)
    fid = fopen(logfilename,'a');
    fn = fns{fn_i};
    format long g
    
    % load SPM.mat for second level
    second_level_mat = load(fn);
    disp(fn);
    fprintf(fid, '%s\n',fn);
    fclose(fid);
    % which con images where used?
    con_images = second_level_mat.SPM.xY.P;
    
    % get unique vps
    vp_idx = strfind(con_images,'vp');
    vps = [];
    for i = 1:size(vp_idx,1)
        vps = [vps; con_images{i}(vp_idx{i}+2:vp_idx{i}+3)];
    end
    vps = unique(vps,'rows');
    
    
    root = '';
    subdir = '';
    
    for i = 1:length(vps)
        fid = fopen(logfilename,'a');
        text = [' vp' vps(i,:)];
        disp(text);
        fprintf(fid, '%s\n',text );
        
        folder = [root vps(i,:) subdir];
        % load subject SPM.mat file
        if fn_i == 1
            subj_spms{i} = load([folder 'SPM.mat']);
        end
        
        % get con images for this subject
        cons_all = strfind(con_images, ['vp' vps(i,:)]);
        cons_subj = [];
        for j = 1:size(cons_all,1)
            if ~isempty(cons_all{j})
                cons_subj = [cons_subj; con_images{j} ];
            end
        end
        
        
        for con =1:size(cons_subj, 1)
            disp(['  ' cons_subj(con,:)]);
            
            % get name and idx for con_*.nii from filename
            con_nr_idx = strfind(cons_subj(con,:),'con_');
            con_nr = str2num(cons_subj(con,con_nr_idx+4:end-6));
            text = ['   '  subj_spms{i}.SPM.xCon(con_nr).name];
            disp(text);
            fprintf(fid, '%s\n', text);
            
            % description of the beta files used to create this con
            beta_subj = {subj_spms{i}.SPM.Vbeta( subj_spms{i}.SPM.xCon(con_nr).c ~= 0).descrip};
            
            for beta = 1:size(beta_subj, 2)
                %        disp(['     ' beta_subj{beta} ]);
                
                % get session nr for this beta
                idx_a = strfind(beta_subj{beta}, 'Sn(');
                idx_b = strfind(beta_subj{beta}, ') ');
                idx_b = idx_b(2);
                ses = str2num(beta_subj{beta}(idx_a+3:idx_b-1));
                idx_bf = strfind(beta_subj{beta}, '*');
                if isempty(idx_bf)
                    idx_bf = length(beta_subj{beta});
                end
                
                % get trial name (this corresponds to the conditions.mat file)
                trial_name = strip(beta_subj{beta}(idx_a+6:idx_bf-1));
                
                % find trial from beta in conditions list
                trials = { subj_spms{i}.SPM.Sess(ses).U.name};
                for ia = 1:size(trials, 2)
                    res = strfind(trials{ia}, trial_name);
                    if ~isempty(res{1})
                        %             disp(subj_spm.SPM.Sess(ses).U(i).ons);
                        text = ['    '  beta_subj{beta} ...
                            ' Name: '  subj_spms{i}.SPM.Sess(ses).U(ia).name{1} ...
                            ' Trials: ' num2str(length( subj_spms{i}.SPM.Sess(ses).U(ia).ons))];
                        disp(text);
                        fprintf(fid, '%s\n',text );
                        %             disp(subj_spm.SPM.Sess(ses).U(i).dur);
                    end
                end
            end
        end
        disp(' ');
        fprintf(fid, '%s\n', ' ');
        fclose(fid);
    end
end
