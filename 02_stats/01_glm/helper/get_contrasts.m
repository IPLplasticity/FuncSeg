function contrasts = get_contrasts(SPMmat, uni, contrast_names, f_contrast_names)
% Creates the contrast indices for given SPM.mat (filename or struct).
% Beta idx are read from SPM.mat file.
%
% SPMmat: struct of loaded SPM.mat file
%        or cell{string} with filename to SPM.mat

disp('Starting contrast calculations');
deriv_idx = 1;

% there is no need for contrast for the singleitem glm
if ~uni
    disp('no need for contrast for the singleitem glm');
    return
end

clear matlabbatch;

% load matfile or get correct filename from variable
if iscell(SPMmat)
    matlabbatch{1}.spm.stats.con.spmmat = SPMmat;
    disp(['Reading ' SPMmat{1}]);
    
    % load mat to get condition names
    SPMmat = load(SPMmat{1});
    
elseif isstruct(SPMmat)
    matlabbatch{1}.spm.stats.con.spmmat = [SPMmat.SPM.swd '/SPM.mat'];
    
else
    error(['Wrong variable type for matfn: ' class(SPMmat)']);
end

% T-tests
contrasts = define_contrasts(SPMmat.SPM.Vbeta, contrast_names, deriv_idx);
for i = 1:size(contrasts,2)
    matlabbatch{1}.spm.stats.con.consess{i}.tcon.name = contrasts(i).name;
    matlabbatch{1}.spm.stats.con.consess{i}.tcon.weights = contrasts(i).con;
    matlabbatch{1}.spm.stats.con.consess{i}.tcon.sessrep = contrasts(i).sessrep;
end


%% F-tests
if nargin == 4
    [contrasts, adddata] = define_f_contrasts(SPMmat.SPM.Vbeta, f_contrast_names, deriv_idx, SPMmat.SPM.swd);
    for i = 1:size(contrasts,2)
        matlabbatch{1}.spm.stats.con.consess{end+1}.fcon.name = contrasts(i).name;
        matlabbatch{1}.spm.stats.con.consess{end}.fcon.weights = contrasts(i).con;
        matlabbatch{1}.spm.stats.con.consess{end}.fcon.sessrep = contrasts(i).sessrep;
    end
end


matlabbatch{1}.spm.stats.con.delete = 1;

% write matlabbatch as .m
fn = [SPMmat.SPM.swd '/mb_contrasts.m'];
write_batch_mfile(matlabbatch,fn, adddata,'contrasts');

spm('defaults', 'FMRI');
spm_jobman('run', matlabbatch);
end


function [contrasts, data] = define_f_contrasts(betas, f_contrast_names, deriv_idx, wd)
% Builds struct with .name, .con, .sessrep fields for given contrasts.
% f_contrast_names = {'Att',{'Att inv', 'Att val','Att cat'} %  1
%     'Sem',{'Sem w','Sem p'}        %  2
%     'Soc',{'Soc fb','Soc tb'}          % 3
%     };
% this will create one fcontrast per task, with one row per condition
contrasts = struct;
data =cell(0);
% for each ftest
for i = 1:size(f_contrast_names,1)
    contrasts(i).name = f_contrast_names{i,1};
    %     disp(contrasts(i).name);
    con = [];
    
    
    % go through conditions
    for j = 1:numel(f_contrast_names{i,2})
        
        % get target condition indices
        r1 = get_idx(f_contrast_names{i,2}{j}, betas, deriv_idx);
        
%         if ~endsWith(lower(f_contrast_names{i,1}), 'rest')
%             % get other conditions indices
%             r2 = [];
%             for othercond_idx = setdiff(1:numel(f_contrast_names{i,2}),j)
%                 r2 = [r2 get_idx(f_contrast_names{i,2}{othercond_idx}, betas, deriv_idx)];
%             end
%             
%         else
%             % get target condition indices
%             r1 = get_idx(f_contrast_names{i,2}{j}, betas, deriv_idx);
%             r2 = get_idx(['REST ' f_contrast_names{i,1}(1:3)], betas, deriv_idx);
%         end
        
        r = zeros(1,numel(betas));
        r(r1) =  1;
%         r(r2) = -1;
        
%         if sum(r) ~= 0
%             if sum(r) > 0
%                 r(r(:) > 0  ) = sum(r(:) < 0) / sum(r(:) > 0);
%             else
%                 r(r(:) < 0  ) = -sum(r(:) > 0) / sum(r(:) < 0);
%             end
%             a = 1;
%         end
        % check for near 0
%         assert(ismembertol(sum(r),0,.01,'DataScale',1))
        con = [con; r;];
        
        %         disp(f_contrast_names{i,2}{j});
        pos = r(r1);
%         neg = r(r2);
        beta_pos = {betas(r1).descrip};
%         beta_neg = {betas(r2).descrip};
        for a = 1:numel(beta_pos)
            str = sprintf('%s - %s: %+1.1f %s (%s)', ...
                contrasts(i).name, f_contrast_names{i,2}{j}, pos(a), beta_pos{a}, wd);
            data{end+1} = str;
            disp(str);
        end
%         for a = 1:numel(beta_neg)
%             str = sprintf('%s - %s: %+1.1f %s (%s)', ...
%                 contrasts(i).name, f_contrast_names{i,2}{j}, neg(a), beta_neg{a}, wd);
%             data{end+1} = str;
%             disp(str);
%         end
    end
    
    contrasts(i).con = con;
    contrasts(i).sessrep = 'none';
end

end

function contrast_idx = get_idx(c, betas, deriv_idx)
% Get index for single contrast name, according to betas.descrip
contrast_idx = strfind(lower({betas.descrip}), lower(c));
if deriv_idx > 0
    bf = strfind({betas.descrip},['bf('  num2str(deriv_idx) ')' ]);
end

%  strfind returns empty cells, so replace with 0...
for idx = 1:numel(betas)
    if size(contrast_idx{idx}) == 0
        contrast_idx{idx} = 0;
    end
    
    if deriv_idx > 0 && size(bf{idx},1) == 0
        bf{idx} = 0;
    end
end

% find indices where beta name is found and for correct deriv_idx
if deriv_idx >0
    contrast_idx = find([contrast_idx{:}] > 0 & [bf{:}] > 0);
else
    contrast_idx = find([contrast_idx{:}] > 0);
end

if isempty(contrast_idx)
    warning(['Contrast is empty: ' c]) ;
end
end

