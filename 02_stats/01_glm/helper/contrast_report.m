function contrast_report(vps)
uni = true;
vps = get_vp_names(vps);


exfor vp = 1:size(vps,1)
    disp(['Removing old files for ' vps(vp,:)]);
    
    % which files to remove
    fn_prefix = ["con"];
    .spm.stats.con.delete
    for prefix = 1:size(fn_prefix,2)
        disp(['   ' fn_prefix{prefix} '*.nii']);
        if uni
            fns = dir(['/data/pt_01994/probands/' vps(vp,:) '/spm/univariate/' fn_prefix{prefix} '*.nii']);
        else
            fns = dir(['/data/pt_01994/probands/' vps(vp,:) '/spm/single_trial_glm/' fn_prefix{prefix} '*.nii']);
        end
        for f = 1:size(fns,1)
            fn = fullfile(fns(f).folder,fns(f).name);
            %             disp(fn);
            delete(fn);
        end
    end
end
disp('Removing files: done');

parfor vp = 1:size(vps,1)
    disp(['Creating contrasts for ' vps(vp,:)]);
    get_contrasts(get_mat(vps(vp,:), uni), uni);
end
report_stat(vps, uni);
end

function mat_fn = get_mat(vp, uni)
if uni
    mat_fn = {['/data/pt_01994/probands/' vp '/spm/univariate/SPM.mat']};
else
    mat_fn = {['/data/pt_01994/probands/' vp '/spm/single_trial_glm/SPM.mat']};
end
end