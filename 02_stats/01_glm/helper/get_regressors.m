function regressors = get_regressors(vp, session, uni)
if uni
    regressors = {['/data/pt_01994/probands/' vp '/spm/rp' num2str(session) '.txt']};
else
    regressors = {['/data/pt_01994/probands/' vp '/spm_mv/rp_a' num2str(session) '_cmrr_mbep2d_bold_fast.txt']};
end
end