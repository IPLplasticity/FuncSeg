function [vps, missing_vps] = get_vp_names(vps)
vps_ = [];
if size(vps,2) == 1 && size(vps,1) ~= 1
    vps = vps';
end
if isnumeric(vps)
    for vp = vps
        vps_ = [vps_; ['vp', sprintf('%02d',vp)]];
    end
    missing_vps = setdiff(1:22,vps);
elseif ischar(vps) || isstring(vps)
    for i = 1:size(vps,1) %vp = vps
        vp = vps(i,:);
        if ~startsWith(vp, 'vp')
            vps_ = [vps_; ['vp', sprintf('%02d',str2double(vp))]];
        else
            vps_ = [vps_; vp];
        end
    end
    missing_vps = 'NaN';
else
    error('Do not understand input');
end
vps = vps_;

end