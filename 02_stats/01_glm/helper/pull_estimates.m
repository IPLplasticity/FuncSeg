function [est, se, ci] = pull_estimates(spm_fn, contrast_idx, coords)
%% Pull parameter estimates, se and ci (90%) from SPM.mat file.

load(spm_fn, 'SPM');

disp(['Pulling estimates from ' SPM.xCon(contrast_idx).name]);

old_dir = cd(fileparts(spm_fn));
% contrast_idx = 1;
% coords = [45,-61,13];
XYZmm = SPM.xVol.M(1:3,:)*[SPM.xVol.XYZ; ones(1,size(SPM.xVol.XYZ,2))]; % unthresholded
[xyz,i] = spm_XYZreg('NearestXYZ',coords,XYZmm);
% spm_XYZreg('SetCoords',xyz,hReg);
XYZ     = SPM.xVol.XYZ(:,i); % coordinates

%-Parameter estimates:   beta = xX.pKX*xX.K*y;
%-Residual mean square: ResMS = sum(R.^2)/xX.trRV
%----------------------------------------------------------------------
beta  = spm_get_data(SPM.Vbeta, XYZ);
ResMS = spm_get_data(SPM.VResMS,XYZ);
Bcov  = ResMS*SPM.xX.Bcov;

ci    = 1.6449;
% compute contrast of parameter estimates and 90% C.I.
%------------------------------------------------------------------
% Ic = SPM.Ic; % Use current contrast
est = SPM.xCon(contrast_idx ).c'*beta;
se    = sqrt(diag(SPM.xCon(contrast_idx ).c'*Bcov*SPM.xCon(contrast_idx ).c));
ci    = ci*se;
cd(old_dir);
