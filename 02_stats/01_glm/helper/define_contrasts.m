function contrasts = define_contrasts(betas, contrast_names, deriv_idx)
% Builds struct with .name, .con, .sessrep fields for given contrasts.
% 
contrasts = struct;

for i = 1:numel(contrast_names)
    contrasts(i).name = contrast_names{i};
    c = strsplit(contrast_names{i}, ' > ');
    
    % [1  0] contrast for var vs rest   
    % [1 -1] contrast elsewhere
    contrasts(i).con = get_contrast_idxs(c, deriv_idx, betas);
    contrasts(i).sessrep = 'none';
    
end
end

function r = get_contrast_idxs(c, deriv_idx, betas)
% Get indices for given contrast names (c{1} and c{2}) and betas.
% 
% if c{2} == 'Rest', contrast exists of 1s at c{1} positions
% else, contrast exists of (1,-1) with sum(contrast) == 0.

if contains(lower(c{2}), 'rest') && ~any(strfind([betas.descrip],'REST'))
    % c2 = ' REST' ;
    r1 = get_idx(c{1}, betas, deriv_idx);
    r = zeros(1, numel(betas));
    r(r1) =  1;

else
    % c1 = ATT inv
    % c2 = ATT val
    r1 = get_idx(c{1}, betas, deriv_idx);
    r2 = get_idx(c{2}, betas, deriv_idx);
    
    r = zeros(1,numel(betas));
    r(r1) =  1;
    r(r2) = -1;
    
    % adjust contrast vector to sum(r) == 1
    if sum(r) ~= 0
        if sum(r) > 0
            r(r(:) > 0  ) = sum(r(:) < 0) / sum(r(:) > 0);
        else
            r(r(:) < 0  ) = -sum(r(:) > 0) / sum(r(:) < 0);
        end
        a = 1;
    end
    % check for near 0
    assert(ismembertol(sum(r),0,.01,'DataScale',1))
end
end

function contrast_idx = get_idx(c, betas, deriv_idx)
% Get index for single contrast name, according to betas.descrip
contrast_idx = strfind(lower({betas.descrip}), lower(c));
if deriv_idx > 0
    bf = strfind({betas.descrip},['bf('  num2str(deriv_idx) ')' ]);
end

%  strfind returns empty cells, so replace with 0...
for idx = 1:numel(betas)
    if size(contrast_idx{idx}) == 0
        contrast_idx{idx} = 0;
    end
    
    if deriv_idx > 0 && size(bf{idx},1) == 0
        bf{idx} = 0;
    end
end

% find indices where beta name is found and for correct deriv_idx
if deriv_idx >0
    contrast_idx = find([contrast_idx{:}] > 0 & [bf{:}] > 0);
else
    contrast_idx = find([contrast_idx{:}] > 0);
end

if isempty(contrast_idx)
    warning(['Contrast is empty: ' c]) ;
end
end