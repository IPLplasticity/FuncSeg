function b_2nd_level(vps)
% This creates one model for each 2nd level contrast

[vps, vp_mis] = get_vp_names(vps);
wd = pwd();
clear matlabbatch;
prefix = '/data/pt_01994/results/univariate/scndlvl/';

folder_name = 'uv_12s_fmriprep_acomp';

res_folder = [prefix folder_name];
if any(vp_mis)
    strrep(num2str(vp_mis),' ','_')
    res_folder = [res_folder '_no' strrep(num2str(vp_mis),'  ','_')];
end

if ~endsWith(res_folder,'/')
    res_folder = [res_folder '/'];
end
mkdir(res_folder);

fn = [res_folder 'info.txt'];

% two sample t-test
scnd_lvl_data_ts(1).dir = 'att';
scnd_lvl_data_ts(1).conditions = {'inv', 'val'};
scnd_lvl_data_ts(1).scan_nrs = {4, 3};


scnd_lvl_data_ts(2).dir = 'sem';
scnd_lvl_data_ts(2).conditions = {'w', 'p'};
scnd_lvl_data_ts(2).scan_nrs = {8, 9};

scnd_lvl_data_ts(3).dir = 'soc';
scnd_lvl_data_ts(3).conditions = {'fb', 'tb'};
scnd_lvl_data_ts(3).scan_nrs = {12,11};


% one sample t-test
scnd_lvl_data_os(1).dir = 'att/os/inv_vs_val';
scnd_lvl_data_os(1).condition = {'inv > val'};
scnd_lvl_data_os(1).scan_nr = 2;
scnd_lvl_data_os(2).dir = 'att/os/val_vs_inv';
scnd_lvl_data_os(2).condition = {'val > inv'};
scnd_lvl_data_os(2).scan_nr = 1;

scnd_lvl_data_os(3).dir = 'sem/os/w_vs_p';
scnd_lvl_data_os(3).condition = {'w > p'};
scnd_lvl_data_os(3).scan_nr = 6;
scnd_lvl_data_os(4).dir = 'sem/os/p_vs_w';
scnd_lvl_data_os(4).condition = {'p > w'};
scnd_lvl_data_os(4).scan_nr = 7;

scnd_lvl_data_os(5).dir = 'soc/os/fb_vs_tb';
scnd_lvl_data_os(5).condition = {'fb vs tb'};
scnd_lvl_data_os(5).scan_nr = 10;

%     '/soc/e_vs_u'
%     '/soc/u_vs_e'
%     13,14
%     14,13
paired = true;
single_t = false;
if single_t
    matlabbatch = cell(0);
    disp('Creating single t-tests')
    writetable(struct2table(scnd_lvl_data_os), fn);
    fid = fopen(fn,'a');
    fprintf(fid, ['\n' datestr(datetime) '\n']);
    
    for i = 1:numel(scnd_lvl_data_os)
        % model specification
        
        matlabbatch{end+1}.spm.stats.factorial_design.dir = {[res_folder scnd_lvl_data_os(i).dir]};
        matlabbatch{end}.spm.stats.factorial_design.des.t1.scans =  get_scans_os(vps, scnd_lvl_data_os(i).scan_nr, folder_name);
        fid = fopen(fn,'a');
        
        fprintf(fid, ['firstlevel folder: ' folder_name '\n']);
        fprintf(fid, ['scans: \n']);
        for sc = 1:numel(matlabbatch{end}.spm.stats.factorial_design.des.t1.scans)
            fprintf(fid, ['\t' matlabbatch{end}.spm.stats.factorial_design.des.t1.scans{sc} '\n'] );
        end
        fprintf(fid, [':\n']);
        
        
        matlabbatch{end}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
        matlabbatch{end}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
        matlabbatch{end}.spm.stats.factorial_design.masking.tm.tm_none = 1;
        matlabbatch{end}.spm.stats.factorial_design.masking.im = 1;
        matlabbatch{end}.spm.stats.factorial_design.masking.em = {''};
        matlabbatch{end}.spm.stats.factorial_design.globalc.g_omit = 1;
        matlabbatch{end}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
        matlabbatch{end}.spm.stats.factorial_design.globalm.glonorm = 1;
        
        % model estimation
        matlabbatch{end+1}.spm.stats.fmri_est.spmmat = {[res_folder scnd_lvl_data_os(i).dir '/SPM.mat']}; % {'/data/pt_01994/results/univariate/scndlvl/att/val_rest_vs_inv_rest/SPM.mat'};
        matlabbatch{end}.spm.stats.fmri_est.write_residuals = 0;
        matlabbatch{end}.spm.stats.fmri_est.method.Classical = 1;
        
        % contrast calculation
        matlabbatch{end+1}.spm.stats.con.spmmat = {[res_folder scnd_lvl_data_os(i).dir '/SPM.mat']}; % {'/data/pt_01994/results/univariate/scndlvl/att/val_rest_vs_inv_rest/SPM.mat'};
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.name = scnd_lvl_data_os(i).condition{1};
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.weights = [1];
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
        
        % results
        matlabbatch{end+1}.spm.stats.results.spmmat ={[res_folder scnd_lvl_data_os(i).dir '/SPM.mat']};
        matlabbatch{end}.spm.stats.results.conspec(1).titlestr = [scnd_lvl_data_os(i).condition{1} ' (.05 FWE)'];
        matlabbatch{end}.spm.stats.results.conspec(1).contrasts = 1;
        matlabbatch{end}.spm.stats.results.conspec(1).threshdesc = 'FWE';
        matlabbatch{end}.spm.stats.results.conspec(1).thresh = 0.05;
        matlabbatch{end}.spm.stats.results.conspec(1).extent = 0;
        matlabbatch{end}.spm.stats.results.conspec(1).conjunction = 1;
        matlabbatch{end}.spm.stats.results.conspec(1).mask.none = 1;
    end
    matlabbatch{end}.spm.stats.results.export{1}.ps=1;% pdf=1 leads to 1 file per page
    
    spm('defaults', 'FMRI');
    spm_jobman('run', matlabbatch);
end

if paired
    disp('Creating paired t-tests');
    writetable(struct2table(scnd_lvl_data_ts), fn);
    matlabbatch = cell(0);
    fid = fopen(fn,'a');
    fprintf(fid, ['\n' datestr(datetime) '\n']);
    
    for i = 1:numel(scnd_lvl_data_ts)
        matlabbatch{end+1}.spm.stats.factorial_design.dir = {[res_folder scnd_lvl_data_ts(i).dir]};
        
        
        % model specification
        matlabbatch{end}.spm.stats.factorial_design.des.pt.pair = get_scans_ts(vps, scnd_lvl_data_ts(i).scan_nrs, folder_name);
        
        
        fprintf(fid, ['firstlevel folder: ' folder_name '\n']);
        fprintf(fid, ['scans: \n']);
        for sc = 1:numel(matlabbatch{end}.spm.stats.factorial_design.des.pt.pair)
            fprintf(fid, ['\t' matlabbatch{end}.spm.stats.factorial_design.des.pt.pair(sc).scans{1} '\n']);
            fprintf(fid, ['\t' matlabbatch{end}.spm.stats.factorial_design.des.pt.pair(sc).scans{2} '\n']);
        end
        fprintf(fid, ['\n']);
        
        matlabbatch{end}.spm.stats.factorial_design.des.pt.gmsca = 0;
        matlabbatch{end}.spm.stats.factorial_design.des.pt.ancova = 0;
        matlabbatch{end}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
        matlabbatch{end}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
        matlabbatch{end}.spm.stats.factorial_design.masking.tm.tm_none = 1;
        matlabbatch{end}.spm.stats.factorial_design.masking.im = 1;
        matlabbatch{end}.spm.stats.factorial_design.masking.em = {''};
        matlabbatch{end}.spm.stats.factorial_design.globalc.g_omit = 1;
        matlabbatch{end}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
        matlabbatch{end}.spm.stats.factorial_design.globalm.glonorm = 1;
        
        % model estimation
        matlabbatch{end+1}.spm.stats.fmri_est.spmmat = {[res_folder scnd_lvl_data_ts(i).dir '/SPM.mat']}; % 
        matlabbatch{end}.spm.stats.fmri_est.write_residuals = 0;
        matlabbatch{end}.spm.stats.fmri_est.method.Classical = 1;
        
        % contrast calculation
        matlabbatch{end+1}.spm.stats.con.spmmat = {[res_folder scnd_lvl_data_ts(i).dir '/SPM.mat']}; % 
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.name = [scnd_lvl_data_ts(i).conditions{1} ' > ' scnd_lvl_data_ts(i).conditions{2}];
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
        matlabbatch{end}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
        %
        matlabbatch{end}.spm.stats.con.consess{2}.tcon.name = [scnd_lvl_data_ts(i).conditions{2} ' > ' scnd_lvl_data_ts(i).conditions{1}];
        matlabbatch{end}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
        matlabbatch{end}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
     
        matlabbatch{end}.spm.stats.con.delete = 0;
        
        % results
        matlabbatch{end+1}.spm.stats.results.spmmat ={[res_folder scnd_lvl_data_ts(i).dir '/SPM.mat']};
        for res =1:2
            matlabbatch{end}.spm.stats.results.conspec(res).titlestr = [matlabbatch{end-1}.spm.stats.con.consess{res}.tcon.name ' (.05 FWE)'];
            matlabbatch{end}.spm.stats.results.conspec(res).contrasts = res;
            matlabbatch{end}.spm.stats.results.conspec(res).threshdesc = 'FWE';
            matlabbatch{end}.spm.stats.results.conspec(res).thresh = 0.05;
            matlabbatch{end}.spm.stats.results.conspec(res).extent = 0;
            matlabbatch{end}.spm.stats.results.conspec(res).conjunction = 1;
            matlabbatch{end}.spm.stats.results.conspec(res).mask.none = 1;
        end
        
        matlabbatch{end}.spm.stats.results.export{1}.ps=1;% pdf=1 leads to 1 file per page
        
    end
    % save matlabbatch
    
    spm('defaults', 'FMRI');
    spm_jobman('run', matlabbatch);
    save([res_folder '/matlabbatch.mat'], 'matlabbatch');
end


fclose(fid);
cd(wd);
end

function pair = get_scans_ts(vps, scans, subfolder)
pair_idx = 1;
a = scans{1};
b = scans{2};
for vp_idx = 1:size(vps,1)
    pair(pair_idx).scans = {
        ['/data/pt_01994/probands/' vps(vp_idx,:) '/spm/' subfolder '/con_' sprintf('%04d',a) '.nii,1']
        ['/data/pt_01994/probands/' vps(vp_idx,:) '/spm/' subfolder '/con_' sprintf('%04d',b) '.nii,1']
        };
    pair_idx = pair_idx+1;
end

end

function scans = get_scans_os(vps, scan, subfolder)

scans = cellstr([...
    repmat('/data/pt_01994/probands/', size(vps,1),1)...
    vps ...
    repmat(['/spm/' subfolder '/con_'],size(vps,1),1)...
    repmat(sprintf('%04d.nii,1', scan), size(vps, 1),1)]);

end
