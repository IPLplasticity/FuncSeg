function a_1st_level(vps_, uni)
% A_1ST_LEVEL does the model specification and estimation, contrasts
% calculation and prints reports with SPM.
% 12 Sessions
% vps: list of vp names
%   Examples:
%   vps = ['vp01', 'vp02', ...]
%   vps = ["vp01"; "vp02", ...]
%   vps = ['1'; '2', ...]
%   vps = [1, 2, ...]
%   vps = 2
%
%
% For each of the 3 sessions, the 4 runs have to be preprocessed en bloc.
% The rp*.txt file per session has to be concated, as well as the
% parameters timings (not run-wise, but epi-session-wise timings).
%
% Expected files:
% Epis:
% /data/pt_01994/probands/vpX/mr/sesY/nifti/swuaZ_cmrr_mbep2d_bold_fast.nii'
%
% /data/pt_01994/probands/vpX/spm/sesY.mat']};
%
% Motion parameters:
% /data/pt_01994/probands/vpX/spm/rpY.txt']};
%
% X = [1:22] | Y = [1:3] | Z = [4;7;10;13]
%
%

% make sure vps are string and starts with 'vp'.
% pause(8*60*60)
vps = get_vp_names(vps_);
if uni
    pref_subdir = '/spm/';
else
    pref_subdir = '/spm_mv/';
end

if uni

     subdir = 'uv_12s_fmriprep_acomp';
else
    subdir = '12s_fmriprep_acomp';
end

mr_dir = '/spm/fmriprepped/';

jobfilename = 'job_files/first_level_uv_12_ses_job.m';
ncpu = 22;
multiprocessing = true; % change parfor to for by hand as well
waitsec = 60;  % 38gb / 5mb/s = 778s

% which parts to run?
rem_files = true;
clear_spmmat = true;
backup_spmmat = true;
model_spec = true;
model_est = true;
calc_contrasts = false;
rep_stat = false;

%% definitions of contrasts
contrast_names = {'Att val > Att inv' %  1
    'Att inv > Att val'          %  2
    'Att val > REST att'         %  3
    'Att inv > REST att'         %  4
    'Att cat > REST att'         %  5
    'Sem word > Sem pseudoword'  %  6
    'Sem pseudoword > Sem word'  %  7
    'Sem word > REST sem'        %  8
    'Sem pseudoword > Rest sem'  %  9
    'Soc fb > Soc tb'            % 10
    'Soc tb > REST soc'          % 11
    'Soc fb > REST soc'          % 12
%     'Soc e > REST soc'           % 13
%     'Soc u > REST soc'           % 14
    };

f_contrast_names = {'Att',{'Att inv', 'Att val'} %  13
    'Sem',{'Sem word','Sem pseudoword'}                    %  14
    'Soc',{'Soc fb','Soc tb'}                              %  15
%     'Att > rest', {'Att inv', 'Att val'}'                  %  4
%     'Sem > rest',{'Sem word','Sem pseudoword'}             %  5
%     'Soc > rest',{'Soc fb','Soc tb'}                       %  6 
    'EOI',{'Att inv', 'Att val','Sem word','Sem pseudoword','Soc fb','Soc tb'}}; % 16

% create new pool if needed with ncpu
if multiprocessing
    p = gcp('nocreate');
    if isempty(p)
        p = parpool(ncpu);
    elseif p.NumWorkers ~= ncpu
        delete(p)
        p = parpool(ncpu);
    end
    
end

%% Removing files
if clear_spmmat
    for vp = 1:size(vps,1) 
        disp(['Moving old SPM.mat file for ' vps(vp,:)]);
        fn = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/SPM.mat'];
        fn_new = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/SPM_' datestr(now,'yymmdd_HHMMSS') '.mat'];
        try
            movefile(fn,fn_new);
        catch e
        end
    end
end

if backup_spmmat
    for vp = 1:size(vps,1) 
        disp(['Moving old SPM.mat file for ' vps(vp,:)]);
        fn = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/SPM.mat'];
        fn_new = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/SPM_' datestr(now,'yymmdd_HHMMSS') '.mat'];
        try
            copyfile(fn,fn_new);
        catch e
        end
    end
end

if rem_files
    for vp = 1:size(vps,1) 
        disp(['Removing old files for ' vps(vp,:)]);
        
        % which files to remove
        fn_prefix = ["beta", "Res", "spmT", "con"];
        try
            for prefix = 1:size(fn_prefix,2)
                disp(['   ' fn_prefix{prefix} '*.nii']);
                
                fns = dir(['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/' fn_prefix{prefix} '*.nii']);
                
                for f = 1:size(fns,1)
                    fn = fullfile(fns(f).folder,fns(f).name);
                    delete(fn);
                end
            end
        catch e
        end
    end
    disp('Removing files: done');
end

%% Model specification & estimation
if model_spec || model_est || calc_contrasts
    parfor (vp = 1:size(vps,1), ncpu) % size(vps,1) == count of vps ---------------------parfor
%             for vp = 1:size(vps,1)
        if multiprocessing && ncpu >= size(vps,1)
            % let some threads wait to speed up disk read
            switch vp
                case num2cell(4:7)
                    pause(waitsec)
                case num2cell(8:11)
                    pause(waitsec*2)
                case num2cell(12:15)
                    pause(waitsec*3)
                case num2cell(16:19)
                    pause(waitsec*4)
                case num2cell(20:22)
                    pause(waitsec*5)
            end
        end
        disp(['Starting ' vps(vp,:)]);
        try
            matfn = get_mat(vps(vp,:), pref_subdir, subdir);
            if model_spec
                n_runs = 12;
                jobfile = {jobfilename};
                jobs = repmat(jobfile, 1, 1);
                inputs = cell(n_runs*3 + 2, 1);
                
                inputs{01, 1} = get_dir(vps(vp,:), pref_subdir, subdir, uni); % fMRI model specification: Directory - cfg_files
                idx = 1;
                for run = 1:n_runs
                    inputs{idx+1, 1} = get_scans(vps(vp,:), run, mr_dir, uni); % fMRI model specification: Scans - cfg_files
                    inputs{idx+2, 1} = get_cond(vps(vp,:), run, pref_subdir, subdir, uni); % parameters
                    inputs{idx+3, 1} = get_regressors(vps(vp,:), run, mr_dir); % regressors
                    idx = idx + 3;
                end
                vps_string = vps(vp,:);
                inputs{end, 1} = {['/data/pt_01994/probands/' vps(vp,:) '/spm/fmriprepped/w_sub-' vps_string(3:4) '_ses-01_fmriprepped_func.nii']}; % mask
                %         inputs{end, 1} = {ser_cor}; % AR or FAST
                
                % save variable
                disp(['Model specification for ' vps(vp,:)]);
                spm('defaults', 'FMRI');
                
                inputs_fn = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/inputs.mat'];
                job_fn = ['/data/pt_01994/probands/' vps(vp,:) pref_subdir subdir '/matlabbatch.m'];
                
                parsave(inputs_fn,inputs);
                copyfile(jobfilename, job_fn);
                
                spm_jobman('run', jobs, inputs{:});
            end
            
            
            %% Model estimations
            if model_est
                disp(['Model estimation for ' vps(vp,:)]);
                est_model(matfn);
            end
            
            %% Get contrasts
            if calc_contrasts             
                disp(['Creating contrasts for ' vps(vp,:)]);
                get_contrasts(matfn, uni, contrast_names, f_contrast_names);
            end
            
            
        catch e
            disp(['ERROR: ' vps(vp,:)]);
            disp(e);
            for k=1:length(e.stack)
                disp(e.stack(k));
            end
        end
    end
end


%% Get tests plots
if rep_stat
    report_stat(vps, subdir, uni);
end

end

%% Helper function
function est_model(matfn)
disp('Starting model estimations');
matlabbatch{1}.spm.stats.fmri_est.spmmat = matfn;
matlabbatch{1}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{1}.spm.stats.fmri_est.method.Classical = 1;
spm_jobman('run', matlabbatch);
end


function mask = get_mask(vp)
mask = {['/data/pt_01994/probands/' vp '/mr/t1/wBrain.nii']};
end


function directory = get_dir(vp, pref_subdir, subdir, uni)
% Where to store subject results
suffix = [pref_subdir subdir];
directory = ['/data/pt_01994/probands/' vp suffix];
if ~exist(directory, 'dir')
    mkdir(directory);
end
directory  = {directory};
end


function scans = get_scans(vp, run, mr_dir, uni)
% Get scanname.nii,* object

if uni
    bold_fast_prefix = "s8w_sub-";
else
    bold_fast_prefix = "w_sub-";
end

bold_fast_suffix = '_fmriprepped_func.nii';


% get number of volumes for each scan
% fn = ['/data/pt_01994/probands/' vp mr_dir char(bold_fast_prefix) num2str(run) bold_fast_suffix];
fn = ['/data/pt_01994/probands/' vp mr_dir char(bold_fast_prefix) vp(3:4) '_ses-' sprintf('%02d',run) bold_fast_suffix];

run_nifti = nifti(fn);
run_vols = run_nifti.dat.dim(4);
clear('run_nifti');
%                     scans{1,i+((ses-1)*self.nrun)} = cellstr([repmat([fn ','],run_vols,1) num2str([1:run_vols]', '%-i')]);
scans = cellstr([repmat([fn ','], run_vols,1) num2str([1:run_vols]', '%-i')]);
end


function conditions = get_cond(vp, run, pref_subdir, subdir, uni)
% Return conditions .mat file filename with .names, .onsets, durations.
% R-version gets transformed to SPM version.

if uni
    org =    ['/data/pt_01994/probands/' vp pref_subdir subdir '/r_run_' num2str(run) '.mat'];
    target = ['/data/pt_01994/probands/' vp pref_subdir subdir '/run_'   num2str(run) '.mat'];
    rmat2spmmat(org, target);
    conditions = {target};
else
    org =    ['/data/pt_01994/probands/' vp pref_subdir subdir '/st'   num2str(run) '.mat'];
    target = ['/data/pt_01994/probands/' vp pref_subdir subdir '/run_' num2str(run) '.mat'];
    rmat2spmmat(org, target);
    conditions = {target};
end
end


function mat_fn = get_mat(vp, pref_subdir, subdir)
% Returns {filename)} to SPM.mat
mat_fn = {['/data/pt_01994/probands/' vp pref_subdir subdir '/SPM.mat']};
end


function regressors = get_regressors(vp, session, mr_dir)
% /data/pt_01994/probands/vp01/spm/fmriprepped/s08_regressors.tsv
regressors = {['/data/pt_01994/probands/' vp mr_dir 's' sprintf('%02d',session) '_regressors_acomp.txt']};

end

function parsave(fn,var)
save(fn,'var');
end
