"""
Effective connectivity between the 4 IPL subregions and the rest of the brain.

Brain parcellated according to Schaefer-Yeo-400 atlas
"""

import os
import numpy as np
import pandas as pd
import nibabel as nib
from nilearn.image import resample_img
from nilearn import datasets as ds
from nilearn.input_data import NiftiLabelsMasker
from nilearn.image import smooth_img
import glob
from scipy.stats import pearsonr

highres = True
cluster_folder = '/data/pt_01994/results/cluster'
conds = ['inv', 'val', 'tb', 'fb', 'pseudow', 'word']  # find unique string, not a subset of 'pseudoword',
beta_path_str = "subjects/sub{0:0>2}/spm/beta_{1:0>4}.nii"
output_dir = "/data/pt_01994/results/func_conn/thresholded/"

for cond in conds:
    print(f"Condition: {cond}")
    subjects = range(1, 23)
    nii_list = []
    for sub in subjects:
        # print(sub)
        beta_fn = "results/mv/beta_csv/betas_vp{0:0>2}_spm.csv"
        df = pd.read_csv(beta_fn.format(sub))

        # avoid redunancy in condition words
        df.spm_desc = df.spm_desc.str.replace('pseudoword', 'pseudow')

        task_df = df[df.spm_desc.str.contains(cond)]

        # restrict to correct cases
        task_df = task_df[task_df.cor]

        if cond == 'word':  # make sure there is no pseudoword in the selection
            inds = np.logical_and(task_df.spm_desc.str.contains('word'),
                                  ~task_df.spm_desc.str.contains('pseudoword'))
            task_df = task_df[inds]
            assert np.all(task_df.spm_desc.str.contains('word').values)
            assert np.any(task_df.spm_desc.str.contains('pseudoword').values) == False

        for i_beta in task_df.beta_nr.values:
            nii_list.append(beta_path_str.format(sub, i_beta))
    niis = smooth_img(nii_list, fwhm=None)

    if highres:
        atlas = nib.load('/data/pt_01994/atlanten/schaefer_400parcels_17mm_1networks.nii.gz')
        atlas_r = resample_img(atlas, target_affine=niis[0].affine, interpolation='nearest')
        assert len(np.unique(atlas_r.get_data())) == 401
    else:
        atlas = nib.load('/data/gesa_ole_LAS/schaefer_100parcels_17networks_1mm.nii.gz')
        atlas_r = resample_img(atlas, target_affine=niis[0].affine, interpolation='nearest')
        assert len(np.unique(atlas_r.get_data())) == 101

    cluster_dict = {'left_pos': cluster_folder + '/cluster_1.nii',
                    'left_ant': cluster_folder + '/cluster_2.nii',
                    'right_ant': cluster_folder + '/cluster_3.nii',
                    'right_pos': cluster_folder + '/cluster_4.nii'}
    for clus in cluster_dict:

        print(f"  Working on {clus}")
        masker_clust = NiftiLabelsMasker(
            labels_img=cluster_dict[clus],

            background_label=0)
        masker_clust.fit()
        cluster_nii = masker_clust.transform(niis)
        #
        # masker_clust_right = NiftiLabelsMasker(
        #     labels_img='/data/gesa_ole_LAS/r.nii.gz',
        #     background_label=0)
        # masker_clust_right.fit()
        # FS_right = masker_clust_right.transform(niis_without_nans)

        suf = '400par' if highres else ''

        masker_atlas = NiftiLabelsMasker(
            labels_img=atlas_r,
            background_label=0)
        masker_atlas.fit()
        atlassed_nii = masker_atlas.transform(niis)

        parcel_rs = np.zeros((400 if highres else 100))
        for i_par, parcel_yeo in enumerate(atlassed_nii.T):
            r, p = pearsonr(parcel_yeo, cluster_nii[:, 0])
            parcel_rs[i_par] = r

        out_nii = masker_atlas.inverse_transform(np.atleast_2d(parcel_rs))
        out_nii.to_filename(output_dir + f'/{cond}_{clus}_{suf}.nii')
