"""
Non-parametric null hypothesis testing on the 3 target conditions.

This generates a permutation-based baseline for effective connectivity between the 4 IPL subregions
and the rest of the brain and thresholds real results by this-

Null hypo: no diff between conditions of interest
Brain parcellated according to Schaefer-Yeo-400 atlas

"""

import os
import glob
import numpy as np
import pandas as pd
import nibabel as nib
from scipy.stats import pearsonr
from nilearn.image import smooth_img
from nilearn.image import resample_img
from scipy.stats import scoreatpercentile
from nilearn.input_data import NiftiLabelsMasker

high_res = True
conds = ['inv', 'fb', 'w']  # find unique string, not a subset of 'pseudoword',
cluster_folder = '/data/pt_01994/results/cluster'
beta_path_str = "subjects/sub{0:0>2}/spm/beta_{1:0>4}.nii"
output_dir = "/data/pt_01994/results/func_conn/baseline/"

subjects = range(1, 23)
nii_list = []

np.random.seed(0)
for sub in subjects:
    beta_fn = "results/mv/beta_csv/betas_vp{0:0>2}_spm.csv"
    df = pd.read_csv(beta_fn.format(sub))

    # avoid redunancy in condition words
    df.spm_desc = df.spm_desc.str.replace('pseudoword', 'pseudow')

    # restrict to correct cases
    task_df = df[df.cor]

    task_df = task_df[
        np.logical_or(np.logical_or(task_df.spm_desc.str.contains('inv'), task_df.spm_desc.str.contains('word')),
                      task_df.spm_desc.str.contains('fb'))]
    min_idx = task_df.groupby('condition').count()['beta_nr'].min()
    print(f"sub {sub:0>2}, taskwise downsample to: {min_idx} trials")
    indices = []
    for cond in conds:
        inds = task_df[task_df['condition'] == cond].index
        indices += np.random.choice(inds, min_idx, replace=False).tolist()

    task_df = task_df.loc[indices]

    for i_beta in task_df.beta_nr.values:
        nii_list.append(beta_path_str.format(sub, i_beta))
print(f'Removing nans from {len(nii_list)} .niis .')
niis_without_nans = smooth_img(nii_list, fwhm=None)

cluster_dict = {'left_pos': cluster_folder + '/cluster_1.nii',
                'left_ant': cluster_folder + '/cluster_2.nii',
                'right_ant': cluster_folder + '/cluster_3.nii',
                'right_pos': cluster_folder + '/cluster_4.nii'}

if high_res:
    atlas = nib.load('atlas/schaefer_400parcels_17mm_1networks.nii.gz')
    atlas_r = resample_img(atlas, target_affine=niis_without_nans[0].affine, interpolation='nearest')
    assert len(np.unique(atlas_r.get_data())) == 401
else:
    atlas = nib.load('atlas/schaefer_100parcels_17networks_1mm.nii.gz')
    atlas_r = resample_img(atlas, target_affine=niis_without_nans[0].affine, interpolation='nearest')
    assert len(np.unique(atlas_r.get_data())) == 101

masker_atlas = NiftiLabelsMasker(
    labels_img=atlas_r,
    background_label=0)
masker_atlas.fit()
SUFFIX = '400par' if high_res else ''
atlassed_nii = masker_atlas.transform(niis_without_nans)

for clus in cluster_dict:
    print(f"Working on {clus}")
    masker_clust = NiftiLabelsMasker(
        labels_img=cluster_dict[clus],
        background_label=0)
    masker_clust.fit()
    cluster_nii = masker_clust.transform(niis_without_nans)

    for permute_idx in range(100):
        parcel_rs = np.zeros((400 if high_res else 100))

        # shuffle indices
        perm_inds = np.arange(len(niis_without_nans))
        np.random.shuffle(perm_inds)
        perm_inds = perm_inds[:int(len(niis_without_nans) / 10)]

        for i_par, parcel_yeo in enumerate(atlassed_nii.T):
            # print(i_par)
            r, p = pearsonr(parcel_yeo[perm_inds], cluster_nii[perm_inds, 0])
            parcel_rs[i_par] = r

        out_nii = masker_atlas.inverse_transform(np.atleast_2d(parcel_rs))
        # out_nii.get_data_dtype()
        #
        # out_nii.set_data_dtype('float32')
        out_nii.to_filename(output_dir + f'/{clus}_{permute_idx}_{SUFFIX}.nii')

for roi_str in ['left_pos', 'left_ant', 'right_pos', 'right_ant']:
    seed_perm_niis = glob.glob(output_dir + roi_str + '*')
    seed_perm_niis = masker_atlas.transform(seed_perm_niis)

    upper_th = scoreatpercentile(np.abs(seed_perm_niis), 95, axis=0)
    upper_th_onesided = scoreatpercentile(seed_perm_niis, 95, axis=0)

    niis_to_th = glob.glob(f'/data/pt_01994/results/func_conn/thresholded//*{roi_str}*{SUFFIX}.nii')
    for nii_to_th in niis_to_th:  # apply TH to maps seeding from that location
        nii_atlas_vals = masker_atlas.transform(nii_to_th)
        nii_atlas_vals_onesided = nii_atlas_vals.copy()

        # one-sided
        print(f"{os.path.split(nii_to_th)[1]}: {np.sum(nii_atlas_vals > upper_th_onesided)} (onesided)")
        nii_atlas_vals_onesided[nii_atlas_vals_onesided < upper_th_onesided] = 0

        out_str = nii_to_th.replace('.nii', '_th0.05_both.nii')
        out_nii = masker_atlas.inverse_transform(nii_atlas_vals_onesided)
        out_nii.to_filename(out_str)

        # two-sided
        print(f"{os.path.split(nii_to_th)[1]}: {np.sum(-nii_atlas_vals > upper_th)}"
              f"/{np.sum((nii_atlas_vals > upper_th))} (neg/pos)")
        nii_atlas_vals[(np.abs(nii_atlas_vals) < upper_th)] = 0

        out_str = nii_to_th.replace('.nii', '_th0.05.nii')
        out_nii = masker_atlas.inverse_transform(nii_atlas_vals)
        out_nii.to_filename(out_str)
