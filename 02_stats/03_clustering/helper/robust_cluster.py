
"""
Some functions to extract a robust cluster solution
"""
import numpy as np
from nilearn import image
from sklearn.neighbors.dist_metrics import DistanceMetric


def get_hemi(img, side):
    img = image.copy_img(img)
    shape4d = img.shape

    # remove other hemispheric data
    if side == 'r':
        if len(shape4d) == 4:
            img.get_data()[:int(shape4d[0] / 2), :, :, :] = 0
        elif len(shape4d) == 3:
            img.get_data()[:int(shape4d[0] / 2), :, :] = 0
        else:
            raise ValueError

    elif side == 'l':
        if len(shape4d) == 4:
            img.get_data()[int(shape4d[0] / 2):, :, :, :] = 0
        elif len(shape4d) == 3:
            img.get_data()[int(shape4d[0] / 2):, :, :] = 0
        else:
            raise ValueError
    else:
        raise ValueError(side)
    return img


def get_cluster_nc(img, max_nc=2, verbose=True):
    shape4d = img.shape

    # remove volumes with more and less clusters than max_nc
    idx_to_remove = np.where(np.max(np.max(np.max(img.get_data(), axis=0), axis=0), axis=0) != max_nc)[0]
    if verbose:
        print(f"{idx_to_remove.shape[0]}/{img.get_data().shape[-1]} removed for nc={max_nc}")
    idx = [i for i in range(shape4d[-1]) if i not in idx_to_remove]
    img = image.index_img(img, idx)

    return image.copy_img(img)


def get_stable_rois(img, edge_val=0, max_neighbor_distance=1., verbose=True):
    """
    Quantify change of border through changes in neighborhood-classes

    original data is present in 4D, but the voxel ~= 0 are numbered 1D (and refer to the 3d data)
    """
    img_data = img.get_data()

    # compute distances (for neighbors) from first volume
    dist_metric = DistanceMetric.get_metric('euclidean')
    # mask_vox = np.where(image.index_img(img, 0).get_data())
    mask_vox = np.where(img_data[:, :, :, 0])
    vox_dist_in_roi = dist_metric.pairwise(np.array(mask_vox).transpose())

    # 3d idx for not-0 elements from first volume
    # data_idx = np.where(img_data[:, :, :, 0])

    # keep an original data, so that the variance between neighbors get computed on that data as well
    img_data_org = np.copy(img_data)

    # for each voxel that is not 0
    cnt = 0
    org_cnt_vox = np.sum(img_data[:, :, :, 0] != 0)
    for vox in range(len(vox_dist_in_roi)):

        # distances from this voxel to all other voxels, in ascending order
        distances = vox_dist_in_roi[vox, vox_dist_in_roi[vox, :].argsort()]

        # get the indices of neighbors to vox in 1d space
        idx = vox_dist_in_roi[vox, :].argsort()[np.where(distances <= max_neighbor_distance)]

        # get classes of these neighboring voxels
        # for all volumes
        # calc class-variance for each voxel over volumnes
        # calc variance of that over voxels
        # if this is not 0, the class border has changes over iterations
        if np.var(np.var(img_data_org[mask_vox][idx], axis=0)):
            img_data[mask_vox[0][vox], mask_vox[1][vox], mask_vox[2][vox], :] = edge_val
            cnt += 1
    if verbose:
        print(f"{cnt}/{org_cnt_vox} voxels removed")
    return img
