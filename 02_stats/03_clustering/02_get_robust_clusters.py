""" Read all cluster results from the random inits and extract robust solution"""
import glob
from .helper.robust_cluster import get_hemi, get_cluster_nc, get_stable_rois
from nilearn import image
from numpy import np

output_folder = "/data/pt_01994/results/cluster"
# read all cluster nii
fns_t = output_folder + "/IPL/ss_att_sem_soc_parcels2-7_kmeans_seed*.nii"
files = glob.glob(fns_t)
files.sort()

all_img = image.load_img(files)

# right hemisphere
img_r = get_hemi(all_img, 'r')
img_r = get_cluster_nc(img_r, 2)
img_r.to_filename(output_folder + "/cluster_r")

img_r_clean = image.index_img(get_stable_rois(img_r), 0)
img_r_clean.to_filename(f"cluster_cleaned_r_all")

# left hemisphere
img_l = get_hemi(all_img, 'l')
img_l = get_cluster_nc(img_l, 2)
img_l.to_filename(output_folder + "/cluster_l")

img_l_clean = image.index_img(get_stable_rois(img_l), 0)
img_l_clean.to_filename(output_folder + "/cluster_cleaned_l_all")
img_l_clean = image.load_img(output_folder + "/cluster_cleaned_l_cleaned.nii.gz")

# combine both hemisphere
max_num = np.max(img_l_clean.get_data())

img_r_clean.get_data()[img_r_clean.get_data().astype(bool)] = \
    img_r_clean.get_data()[img_r_clean.get_data().astype(bool)] + max_num

cluster_img = image.math_img("img1 + img2", img1=img_r_clean, img2=img_l_clean)
cluster_img.to_filename(output_folder + "/"
                        f"cluster_combined")

# save single cluster images
for i in range(1, 5):
    clus_tmp = image.copy_img(cluster_img)
    data = clus_tmp.get_fdata()
    clus_tmp.to_filename(output_folder + f"/cluster_{i}")
