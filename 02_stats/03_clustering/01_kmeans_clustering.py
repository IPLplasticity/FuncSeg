"""Apply k-means and ward clustering to univariate results

TODO: implement stability
"""
import collections
import subprocess
import tempfile
import warnings
from functools import partial
from multiprocessing.pool import Pool
import numpy as np
from nilearn import masking, image
from nilearn.plotting import plot_glass_brain
import nilearn as nl
import yaml
import os
import pandas as pd
from sklearn.neighbors._dist_metrics import DistanceMetric

warnings.filterwarnings("ignore", message='Casting data from')

params = {'subjects': list(range(1, 23)),
          'rois':
              {"IPL": ["PFcm", "PFop", "PF", "PFm", "PFt", "PGa", "PGp"]},
          'random_state': 1,
          'atlas_folder': "/data/pt_01994/atlanten/IPL/",
          'n_parcels': range(2, 8),
          'methods': ["kmeans"],
          'resfolder': "/data/pt_01994/results/mv/cluster_acomp_deriv/",
          'standardize': True
          }


def get_clust_metric(nii_img, mask_img, out_pref='clus', dist=True,
                     method="ward.D2", index="all_but_graph",
                     nc_min=2, nc_max=7, seed=1, save_data=False,
                     verbose=True):
    """
    Wrapper to call R's nbclust

    Parameters:
    -----------
        nii_img: beta.nii img
        mask_img: mask
        out_pref: filename prefix for tempfile (??)
        dist: bool, provide distance information between voxels
        method: ward.D2, ward, kmeans
        index:  all, alllong, all_but_graph, alllong_but_graph
        nc_min, nc_max: number of cluster
        seed: int,
        save_data: bool, str, keep result files from R.
        verbose: bool

    """

    script_fn = "helper/cluster_metrics.R"
    with tempfile.TemporaryDirectory() as temp_dir:
        if save_data:
            temp_dir = save_data

        # for the R nbclus evaluation, we need the voxel-wise distances.
        # sklearn/nilearn works with adjancy matrices
        dist_metric = DistanceMetric.get_metric('euclidean')
        mask_vox = np.where(mask_img.get_data())
        vox_dist_in_roi = dist_metric.pairwise(np.array(mask_vox).transpose())

        if dist:
            dist_fn = f"{temp_dir}{os.sep}{out_pref}_distances.npy"
            np.save(dist_fn, vox_dist_in_roi)
        else:
            dist_fn = 'euclidean'
            warnings.warn(".nii is flattened to 2D, so euclidean distance is not recommended.")

        if not out_pref.startswith(os.sep):
            out_pref = os.sep + out_pref
        nc_min = str(nc_min)
        nc_max = str(nc_max)
        seed = str(seed)
        data_fn = temp_dir + os.sep + "nii_data.npy"
        np.save(data_fn, nii_img.get_data()[mask_img.get_data().astype(np.bool)])
        out_pref = temp_dir + out_pref

        # environment is needed for MKL <-> numpy
        env = os.environ.copy()

        # call rscript
        if verbose:
            ret = subprocess.run(["R+", "Rscript", script_fn, data_fn, dist_fn, out_pref, method, index,
                                  nc_min, nc_max, seed], env=env)
        else:
            ret = subprocess.run(["R+", "Rscript", script_fn, data_fn, dist_fn, out_pref, method, index,
                                  nc_min, nc_max, seed], stdout=subprocess.DEVNULL, env=env)

        if ret.returncode:
            raise RuntimeError("{} exited with {}".format(script_fn, ret.returncode))

        res = {'best_nc': pd.read_csv(out_pref + "_best_nc.csv"),
               'all_ind': pd.read_csv(out_pref + "_all_ind.csv"),
               'all_crit': pd.read_csv(out_pref + "_all_crit.csv"),
               'best_part': np.load(out_pref + '_best_part.npy')}
    return res


def main(params, rs, **kwargs):
    params['random_state'] = rs
    if type(params['random_state']) is not list:
        params['random_state'] = [params['random_state']]
    for key, val in kwargs.items():
        params[key] = val

    spm_dir = "uv_12s_fmriprep_acomp_deriv"
    subjects_cons = collections.OrderedDict()

    subjects_cons['att'] = [f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0004.nii",
                            f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0003.nii"]
    subjects_cons['sem'] = [f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0008.nii",
                            f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0009.nii"]
    subjects_cons['soc'] = [f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0012.nii",
                            f"/data/pt_01994/probands/vp{{0:0>2}}/spm/{spm_dir}/con_0011.nii"]

    cons_combs = [{cond: subjects_cons[cond]} for cond in subjects_cons.keys()]
    cons_combs += [subjects_cons]
    cons_combs = [subjects_cons]
    columns = ["roi", "task", "side", "parcels", "rand", "score"]
    results = pd.DataFrame(columns=columns)
    for roi in params['rois'].keys():
        try:
            os.makedirs(params["resfolder"] + roi)
        except OSError:
            pass

    roi_fn_t = params['atlas_folder'] + "{}_{}.nii"
    res_fn = params['resfolder'] + "{}/ss_{}_{}_{}_seed{:0>3}.{}"

    for subjects_cons in cons_combs:
        cons = []
        tasks = "_".join(subjects_cons.keys())

        for sub in params['subjects']:
            for task in subjects_cons.keys():
                # add contrast image for condition of interest
                cons.append(subjects_cons[task][0].format(sub))

        cons = image.smooth_img(cons, fwhm=None)

        # iterate over the different rois
        for roi, single_rois in params['rois'].items():

            n_parcel = params['n_parcels']
            parcellist = "parcels{}-{}".format(min(n_parcel), max(n_parcel))

            for method in params['methods']:  # kmeans, ward
                for random_state in params['random_state']:
                    img = None
                    labels_img = None
                    params['score'] = {}
                    for side in ["L", "R"]:

                        # setup sum-roi image from the single rois
                        mask_img = image.load_img(roi_fn_t.format(single_rois[0], side))  # first single roi
                        for roi_fn in single_rois[1:]:  # rest single rois
                            roi_img = image.resample_to_img(roi_fn_t.format(roi_fn, side), mask_img)
                            roi_img.get_data()[roi_img.get_data() > 0.1] = 1
                            roi_img.get_data()[roi_img.get_data() < 0.1] = 0
                            mask_img = image.math_img("img1 + img2", img1=mask_img, img2=roi_img)
                        mask_img.get_data()[mask_img.get_data() != 0] = 1
                        # mask_img.to_filename(f"{params['resfolder']}mask_{roi}.nii.gz")
                        mask_img = image.resample_to_img(mask_img, cons)
                        mask_img.get_data()[mask_img.get_data() < .5] = 0
                        mask_img.get_data()[mask_img.get_data() > .5] = 1

                        if params['standardize']:
                            cons_mask = masking.compute_background_mask(cons)
                            mask_img.get_data()[cons_mask.get_data() == 0] = 0
                            masker = nl.input_data.NiftiMasker(mask_img, standardize=True)
                            cons_stand = masker.fit_transform(cons)
                            betas_img = masker.inverse_transform(cons_stand)
                        else:
                            betas_img = cons

                        # save resampled mask and data
                        mask_img.to_filename(f"{params['resfolder']}{roi}_{side}_mask.nii")
                        betas_img.to_filename(f"{params['resfolder']}{roi}_{side}_data.nii")

                        # get cluster information from R nbclus
                        metric = get_clust_metric(betas_img, mask_img, method=method, index="all_but_graph",
                                                  nc_min=np.min(n_parcel), nc_max=np.max(n_parcel), seed=random_state)

                        # save nclus information
                        metric['all_ind'].to_csv(res_fn.format(roi, tasks + 'all_ind_' + side,
                                                               parcellist, method, random_state, 'csv'))
                        metric['best_nc'].to_csv(res_fn.format(roi, tasks + 'best_nc_' + side,
                                                               parcellist, method, random_state, 'csv'))

                        # combine best cluster solution for left and right hemisphere
                        if not labels_img:
                            labels_img = image.copy_img(mask_img)
                            labels_img.get_data()[:] = 0
                        labels_img.get_data()[mask_img.get_data().astype(np.bool)] = metric['best_part']

                        print("{0: >7} {1: >11} {2} "
                              "parcels: {3}, "
                              "{4}, "
                              "rand: {5:0>3}, "
                              "score: {6:5.2f}".format(roi,
                                                       tasks,
                                                       side,
                                                       metric['best_nc']['all'][0],
                                                       method,
                                                       random_state,
                                                       metric['best_nc']['all'][1]))

                        results.loc[len(results)] = [roi, tasks, side, metric['best_nc']['all'][0], random_state,
                                                     metric['best_nc']['all'][1]]

                        params['best_nc_' + side] = metric['best_nc']['all'][0]
                        params['best_nc_rat_' + side] = metric['best_nc']['all'][1]
                        # if not img:
                        #     img = cluster.labels_img_
                        # else:
                        #     img = image.math_img(formula="img1 + img2", img1=img, img2=cluster.labels_img_)

                    labels_img.to_filename(res_fn.format(roi, tasks, parcellist, method, random_state, 'nii'))

                    plot_glass_brain(labels_img, display_mode='lzr',
                                     output_file=res_fn.format(roi, tasks, parcellist, method,
                                                               random_state, 'png'),
                                     title="{} {} " \
                                           "{: >7}, " \
                                           "L: {:1.0f} ({:1.2f}), "
                                           "R: {:1.0f} ({:1.2f})"
                                           "".format(roi,
                                                     tasks,
                                                     method,
                                                     params['best_nc_L'],
                                                     params['best_nc_rat_L'],
                                                     params['best_nc_R'],
                                                     params['best_nc_rat_R'])
                                     )
                    with open(res_fn.format(roi, tasks, parcellist, method,
                                            random_state, 'yaml'), 'w') as f:
                        yaml.dump(params, f)

                    # if not os.path.exists(f"/data/pt_01994/results/mv/cluster_acomp/{spm_dir}"):
                    #     os.makedirs(f"/data/pt_01994/results/mv/cluster_acomp/{spm_dir}")
                    results.to_csv(res_fn.format(roi, tasks, parcellist, method,
                                                 random_state, 'results.csv'))


if __name__ == "__main__":
    # call 1000 times with random kmeans inits
    randoms = range(1000)
    f = partial(main, params)
    p = Pool(processes=50)
    p.map(f, randoms)
