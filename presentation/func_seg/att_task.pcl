# this is the posner task adapted from corbetta 2000
# two boxes, valid, invalid, null trials


# read att conditions from files, one file per vp, session, run
#array<string> column_names[2]; 
array<string> atttt[n_runs][n_att]; # attention trial type
array<bool> attts[n_runs][n_att]; # attention trial side (left)
array<int> attt_t_fixed[n_runs][n_att];
array<int> attt_t_cue[n_runs][n_att];

loop run_nr = 1 until run_nr > n_runs begin
	
	#input_file in = new input_file;
	in.open( "subjects\\" + vp + "_att_" + string(session) + "_" + string(run_nr) + ".csv" );
	count = 0;
	#in.get_line(); # throw away header

	loop until
		in.end_of_file() || !in.last_succeeded() || count == n_att
	begin
		count = count + 1;
		
		in.get_line().split(",", temp);
		atttt[run_nr][count] = temp[1];

		attts[run_nr][count] = bool(int(temp[2]));
		attt_t_fixed[run_nr][count] = int(temp[3]);
		attt_t_cue[run_nr][count] = int(temp[4]);
		
	end;  
	in.close();
	run_nr = run_nr + 1;
	#term.print_line(atttt);
end;

sub double rad2deg (double rad)
# returns degree from radians
begin
	double deg = rad * (180 / pi_value);
	return deg;
end;

sub double deg2rad (double deg)
# returns radians from degree
begin
	double rad = deg * (pi_value / 180);
	return rad;
end;

sub int getPixLen(double angle, double d, double res, double size) 
# compute pixel length according to viewing angle, distance and resolution
#
# arguments:
# angle: aimed viewing angle 
# 		d: distance subject - monitor
#   res: pixel resolution for dimenson of object
#  size: monitor size in cm in this dimension
#
# returns:
#   len: len in pixel according to angle
begin
		bool verbose = true; # print deviation information of viewing angle
		double deg_per_px = rad2deg(arctan2(0.5*size,d)) / (0.5*res);
		# term.print(deg_per_px);
		double len_prec = angle / deg_per_px;
		int len = int(round(len_prec,0));
		if verbose then
				double dif_px = len_prec - len;
				double dif_ang = dif_px - deg_per_px;
				term.print("Angle: " + string(angle) + ", px: " + string(len) + ", angle deviation of " + string(round(dif_ang,2)) + ".\n");
		end;
		return len;
end;

# rushworth 2001:
# central cross: .88°
# boxes: 2.6° width, centers 8.3° left/right, 6.1° above/below
# box center turning red: 1.3°
# pre-cue green, 250ms or 350ms (50%)
# pre-cue 75% correct, 20% opposite, 5% catch, no target, no button

sub setAttTrialType(string type, bool left, int t_fix, int t_cue, int boxMid, int cross_width, int arrow_len_x, int arrow_len_y, string ec, int j_loc)
# sets att_trial to type = (val,inv,cat) and cue-side (left, not left)
#
# Arguments:
# type: val_id, inv_alid, cat_ch
# left: true, false
# t_fix: fixation time
# t_cue: cue time
begin
	# get position parameters here
	int star_x;
	if left then
		star_x = -boxMid;
	else
		star_x = boxMid;
	end;
	
	# set timinges
	event_boxes_start.set_event_code(ec + "_att_" + string(j_loc) + "_fix_" + type + "_" + string(int(left)) + "_tfix_" + string(t_fix)+ "_tcue_" + string(t_cue));
	
	event_boxes_cue.set_deltat(t_fix);
	
	event_boxes_cue.set_event_code(ec + "_att_" + string(j_loc) + "_cue_" + type + "_" + string(int(left)) + "_tfix_" + string(t_fix)+ "_tcue_" + string(t_cue));
	
	event_boxes_target.set_deltat(t_cue);
	#term.print_line(t_cue);
	event_boxes_target.set_event_code(ec + "_att_" + string(j_loc) + "_tar_" + type + "_" + string(int(left)) + "_tfix_" + string(t_fix)+ "_tcue_" + string(t_cue));
	
	event_boxes_fix_end.set_event_code(ec + "_att_" + string(j_loc) + "_end_" + type + "_" + string(int(left)) + "_tfix_" + string(t_fix)+ "_tcue_" + string(t_cue));
	
	#fix_cross.clear();
	#fix_cross.set_next_line_width(5);
	#fix_cross.add_line( -cross_width, 0, 				cross_width, 0);
	#fix_cross.add_line( 0, 				-cross_width, 0, 			 cross_width);
	#fix_cross.redraw();
	
	if type == "val" then
		#fix_cross.add_line( -cross_width, 0, 				cross_width, 0);
		star_bmp.set_filename("stimuli/att/star.png");
		star_bmp.load();
		
		if left then
			# set the arrow to left
			arrow.set_coordinates(cross_width,0,-cross_width,0);
			#arrow.set_size(10,20);
			boxes_cue.set_part_x(5,-cross_width);
			boxes_target.set_part_x(6,-cross_width);
			arrow.redraw();
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, arrow_len_y);
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, -arrow_len_y);
			#fix_cross.add_line( -cross_width + arrow_len_x, arrow_len_y, -cross_width + arrow_len_x, -arrow_len_y);
			
		else
			# set arrow to right
			arrow.set_coordinates(-cross_width, 0, cross_width, 0);
			boxes_cue.set_part_x(5, cross_width);
			boxes_target.set_part_x(6, cross_width);
			arrow.redraw();
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, arrow_len_y);
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, -arrow_len_y);
			#fix_cross.add_line( cross_width - arrow_len_x, arrow_len_y, cross_width -arrow_len_x, -arrow_len_y);
		end;
		
		# set star position 
		boxes_target.set_part_x(5, star_x);
		
		# set color of target star
		#target_text.set_font_color(255,255,255);
		
		event_boxes_target.set_target_button(int(!left));
		
	elseif type == "inv" then
		star_bmp.set_filename("stimuli/att/star.png");
		star_bmp.load();
		
		#fix_cross.add_line( -cross_width, 0, 				cross_width, 0);
		
		if left then 
			# set arrow to right
			arrow.set_coordinates(-cross_width, 0, cross_width, 0);
			boxes_cue.set_part_x(5, cross_width);
			boxes_target.set_part_x(6, cross_width);
			arrow.redraw()
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, arrow_len_y);
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, -arrow_len_y);
			#fix_cross.add_line( cross_width - arrow_len_x, arrow_len_y, cross_width -arrow_len_x, -arrow_len_y);
		
		else
			# set the arrow to left
			arrow.set_coordinates(cross_width, 0, -cross_width, 0);
			boxes_cue.set_part_x(5, -cross_width);
			boxes_target.set_part_x(6, -cross_width);
			arrow.redraw()
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, arrow_len_y);
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, -arrow_len_y);
			#fix_cross.add_line( -cross_width + arrow_len_x, arrow_len_y, -cross_width + arrow_len_x, -arrow_len_y);
		end;
		
		# set star position 
		boxes_target.set_part_x(5, star_x);
		
		# set color of target star
		#target_text.set_font_color(255,255,255);

		event_boxes_target.set_target_button(int(left));
		
	elseif type == "cat" then
		star_bmp.set_filename("stimuli/att/star_black.png");
		star_bmp.load();
		
		if left then
			# set the arrow to left
			arrow.set_coordinates(cross_width, 0, -cross_width,0);
			boxes_cue.set_part_x(5, -cross_width);
			boxes_target.set_part_x(6, -cross_width);
			arrow.redraw()
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, arrow_len_y);
			#fix_cross.add_line( -cross_width, 0, -cross_width + arrow_len_x, -arrow_len_y);
			# no star
		else
			# set arrow to right
			arrow.set_coordinates(-cross_width, 0, cross_width, 0);
			boxes_cue.set_part_x(5, cross_width);
			boxes_target.set_part_x(6, cross_width);
			arrow.redraw()
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, arrow_len_y);
			#fix_cross.add_line( cross_width, 0, cross_width - arrow_len_x, -arrow_len_y);
			# no star
		end;
		
		# set color of target star to black to hide it
		#target_text.set_font_color(0,0,0);
		#ttarget_text.redraw();
		
		# check how to set a null target button
		event_boxes_target.set_target_button(0);
	else
		term.print_line("Warning: " + type + " not recognized.");
	end;
	
	#target_text.redraw();
	#fix_cross.redraw();
end;

# set attention task stimuli to correct visual angles
