# SDL header
response_matching = simple_matching;
active_buttons = 3;
pcl_file = "func_seg_fmri_training.pcl";
scenario_type = fMRI_emulation; # <-----------------------------------------------------------
pulse_code = 255;
pulses_per_scan = 36; # <--------------????????????????
scan_period = 500;
begin;

# SDL body -------------------------------------
$resttime = 16000;

# general purpose start trial
#  used to get some vl input
text{ caption = "Gleich geht es los..."; font_size=24;} start_text;
picture {
	background_color = 0,0,0;
	text start_text;
	x=0;y=100;
} start_pic;

trial {
	#time = 0;
   trial_type = specific_response;
	trial_duration = forever;
	terminator_button = 3;
	stimulus_event {
		picture start_pic;
		} start_event;
} start_trial;


# task information trial. set text with helper.set_info_text()
trial {
	trial_duration = $resttime;
	trial_type = fixed;
	stimulus_event{
		picture {
			text {caption = " "; font_size=24;} info_task_text;
			x = 0;
			y = 0;
			} att_pic;
		} info_event;
	} info_task;
	
bitmap {filename = "stimuli\\fix.tif";} fix_bmp;

text {caption= "+"; font_size = 60;} fix;

trial {
	trial_type = "fixed";
	stimulus_event {
		# picture {bitmap fix_bmp;
		picture {
		text fix;
		x=0; y=0;} fix_pic;
		duration = 3500;
	} fixation_event;
} fixation_trial;

# semantic task trial --------------------------------------------
# a fixation cross before and after the text
# all responses are recorded all the time
trial {
	trial_type = "fixed";
	stimulus_event {
		picture fix_pic;
		duration = next_picture; # set sem_text_event.set_deltat()
	} sem_fix_event_before;

	stimulus_event {
		picture {
				text{
					caption=" " ;
					font_size = 60;} sem_trial_text;
			x=0; y=0;};
	} sem_text_event;

	stimulus_event {
		picture fix_pic;
			deltat = 1000; # show word/pseudoword for 1s
	} sem_fix_event_after;
	
} sem_trial;


# attention task stuff ---------------------------------------
# attention trial template
box {
	height = 100;
	width = 100;
	color= 255,255,255;
} white_box;

box {
	height = 80;
	width = 80;
	color= 0,0,0;
} black_box;

line_graphic {
} fix_cross;

#text {caption = "X";} 
#	target_text;

arrow_graphic {
	head_width = 40;
	head_length = 20;
	line_width = 5; # size is set by coordinates
	width = 100;
	height = 100;}
	arrow;


# boxes_start
picture {
	box white_box;
	x = -200; # set_part_x(1,200);
	y = 0; # set_part_y(1,200);

	box white_box;
	x = 200;
	y = 0;
	
	box black_box;
	x = -200;
	y = 0;
	
	box black_box;
	x = 200;
	y = 0;
	
	line_graphic fix_cross;
	x =0;
	y = 0;
	
} boxes_start;


# boxes_cue
picture {
	box white_box;
	x = -200; # set_part_x(1,200);
	y = 0; # set_part_y(1,200);

	box white_box;
	x = 200;
	y = 0;
	
	box black_box;
	x = -200;
	y = 0;
	
	box black_box;
	x = 200;
	y = 0;
	
	#line_graphic fix_cross;
	#x = 0;
	#y = 0;
	
	arrow_graphic arrow;
	x = 0;
	y = 0;
	
} boxes_cue;


# boxes_target
picture {
	box white_box;
	x = -200; # set_part_x(1,200);
	y = 0; # set_part_y(1,200);

	box white_box;
	x = 200;
	y = 0;
	
	box black_box;
	x = -200;
	y = 0;
	
	box black_box;
	x = 200;
	y = 0;
	
	bitmap {filename = "stimuli/att/star.png";} star_bmp;
	x = 0; y=0;

	arrow_graphic arrow;
	x = 0;
	y = 0;
} boxes_target;


trial { # att_trial
	#trial_duration = 4000;
	#all_responses = false;
	trial_type = fixed;
	
	# start
	stimulus_event{
		picture boxes_start;
		#time = 0;
		code = "fix_att";
	} event_boxes_start;
	
	# cue
	stimulus_event{
		picture boxes_cue;
		deltat = 500;  # this is changed for each trial
		#fix_cross.clear();
		#fix_cross.add_line( -400, 0, 400, 0);
	} event_boxes_cue;
	
	# target
	stimulus_event{
		picture boxes_target;
		#target_button = 1; # this is changed for each trial
		deltat = 1000; # this is changed for each trial
		#duration = 2000;

	} event_boxes_target;
	
	stimulus_event{ # hm, this removes the flickering between trials
		picture boxes_start;
		#time = 0;
		#code = "fix_att";
		deltat = 2000;
	} event_boxes_fix_end;
} att_trial;


#text {caption = "text";} testtext;
# social task stuff --------------------------------------------------
trial { 
	# set trial lenght before call to set fixation time at the end
	trial_type = fixed;
	stimulus_event {
		picture {
			bitmap {preload = false;} soc_bmp1;
			x = 0; y=0;
			
#			text {caption = "1";
#		font_size = 50;} soc_text1;
#			x = 0; y=0;
			} soc_pic1;
		time = 0;
		duration = next_picture;
	} event_soc1;
	
	stimulus_event {
		picture {
			bitmap {preload = false;} soc_bmp2;
#			x = 0; y=0;
#						text {caption = "2";
#		font_size = 50;} soc_text2;
		x = 0; y=0;
			} soc_pic2;
	} event_soc2;
	
	stimulus_event {
		picture {
			bitmap {preload = false;} soc_bmp3;
			x = 0; y=0;
#						text {caption = "3";
#		font_size = 50;} soc_text3;
#		x = 0; y=0;
			} soc_pic3;
	} event_soc3;
		
	stimulus_event {
		picture fix_pic;
	} event_soc4;
} soc_trial;
