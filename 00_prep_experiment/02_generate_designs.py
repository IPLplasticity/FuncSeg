#!/usr/bin/env python
"""Generate optimized designs with neurodesign."""

import os
import sys
import time
import datetime
import argparse
import warnings

warnings.filterwarnings("ignore")

os.environ["MKL_NUM_THREADS"] = "4"
os.environ["NUMEXPR_NUM_THREADS"] = "4"
os.environ["OMP_NUM_THREADS"] = "4"

if 'TASK_UID' in list(os.environ.keys()):
    sys.path.append('/usr/local/bin/')
    from neurodesign import experiment, optimisation

else:
    from neurodesign import experiment, optimisation


def main(argv):
    n_vp = 22
    n_runs = 12

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pre", default=10000)
    parser.add_argument("-c", "--cycles", default=10000)
    parser.add_argument("-r", "--resolution", default=.1)
    parser.add_argument("-f", "--start", default=0)
    parser.add_argument("-t", "--to", default=n_vp * n_runs)
    parser.add_argument("-s", "--seed", default=-1)
    parser.add_argument("--hardprob", default=1)
    parser.add_argument("--cond", default='all')
    args = parser.parse_args()

    resolution = float(args.resolution)
    start = int(args.start)
    cycles = int(args.cycles)
    preruncycles = int(args.pre)
    seed = int(args.seed)
    forced_prob = bool(args.hardprob)
    cond = args.cond

    if seed == -1:
        seed = None
    end = int(args.to)

    if not (cond.lower() == "all" or cond.lower() == "sem" or cond.lower() == "att" or cond.lower() == "soc"):
        raise NotImplementedError

    base_folder = "/data/pt_01756/user/ole/FuncSeg/designs/"

    # build the 3 experiments
    sem_exp = experiment(
        TR=0.5,
        n_trials=40,
        P=[0.5, 0.5],
        C=[[1.0, 0.0], [0.0, 1.0], [0.5, -0.5]],
        duration=None,
        n_stimuli=2,
        rho=0.3,
        resolution=resolution * 2.5,
        stim_duration=1.0,
        restnum=0,
        restdur=0.0,
        ITImodel='exponential',
        ITImin=2.0,
        ITImean=4.5,
        ITImax=7.0,
        confoundorder=3,
        maxrep=8,
        hardprob=forced_prob,
        t_pre=0.0,
        t_post=0.0,
    )

    att_exp = experiment(
        TR=0.5,
        n_trials=40,
        P=[0.75, 0.2, 0.05],
        C=[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [0.5, -0.5, 0.0], [0.5, 0.0, -0.5], [0.0, 0.5, -0.5]],
        duration=None,
        n_stimuli=3,
        rho=0.3,
        resolution=resolution,
        stim_duration=0.5,
        restnum=0,
        restdur=0.0,
        ITImodel='exponential',
        ITImin=2.0,
        ITImean=3.5,
        ITImax=7.0,
        confoundorder=3,
        maxrep=8,
        hardprob=forced_prob,
        t_pre=0.3,
        t_post=0.0,
    )

    soc_exp = experiment(
        TR=0.5,
        n_trials=12,
        P=[0.25, 0.25, 0.25, 0.25],
        C=[[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0],
           [0.5, -0.5, 0.0, 0.0], [0.5, 0.0, -0.5, 0.0], [0.5, 0.0, 0.0, -0.5], [0.0, 0.5, -0.5, 0.0],
           [0.0, 0.5, 0.0, -0.5], [0.0, 0.0, 0.5, -0.5]],
        duration=None,
        n_stimuli=4,
        rho=0.3,
        resolution=resolution * 2.5,
        stim_duration=2.0,
        restnum=0,
        restdur=0.0,
        ITImodel='exponential',
        ITImin=2.0,
        ITImean=3.5,
        ITImax=7.0,
        confoundorder=3,
        maxrep=6,
        hardprob=forced_prob,
        t_pre=4.0,
        t_post=0.0
    )

    # loop over vp*runs
    for i in range(start, end):
        if seed:
            loc_seed = seed
        else:
            loc_seed = start

        folder = base_folder + "sem" + str(i).zfill(3) + "_" + str(preruncycles).zfill(5) + "_" + str(cycles).zfill(
            5) + "_" + str(resolution) + "_fp_" + str(forced_prob) + "/"
        print("\nEvolving " + str(i).zfill(3) + " from " + str(end))
        if not os.path.isfile(folder + "report.pdf") and (cond.lower() == "all" or cond.lower() == "sem"):
            print("\t SEM: " + str(preruncycles) + ", " + str(cycles) + ", " + str(resolution * 2.5))
            start_time = time.time()
            sem_pop = optimisation(
                experiment=sem_exp,
                G=20,
                R=[0, 1, 0],
                q=0.01,
                weights=[0.0, 0.5, 0.25, 0.25],
                I=4,
                preruncycles=preruncycles,
                cycles=cycles,
                convergence=1000,
                folder=folder,
                outdes=1,
                Aoptimality=True,
                optimisation='GA',
                seed=loc_seed
            )
            sem_pop.optimise()
            sem_pop.download()
            print(("\t SEM: " + str(datetime.timedelta(0, time.time() - start_time)) + "\n"))

        folder = base_folder + "att" + str(i).zfill(3) + "_" + str(preruncycles).zfill(5) + "_" + str(cycles).zfill(
            5) + "_" + str(resolution) + "_fp_" + str(forced_prob) + "/"

        if not os.path.isfile(folder + "report.pdf") and (cond.lower() == "all" or cond.lower() == "att"):
            print("\t ATT: " + str(preruncycles) + ", " + str(cycles) + ", " + str(resolution))
            start_time = time.time()
            att_pop = optimisation(
                experiment=att_exp,
                G=20,
                R=[0.4, 0.4, 0.2],
                q=0.01,
                weights=[0.0, 0.5, 0.25, 0.25],
                I=4,
                preruncycles=preruncycles,
                cycles=cycles,
                convergence=1000,
                folder=folder,
                outdes=1,
                Aoptimality=True,
                optimisation='GA',
                seed=loc_seed
            )
            att_pop.optimise()
            att_pop.download()
            print(("\t ATT: " + str(datetime.timedelta(0, time.time() - start_time)) + "\n"))

        folder = base_folder + "soc" + str(i).zfill(3) + "_" + str(preruncycles).zfill(5) + "_" + str(cycles).zfill(
            5) + "_" + str(resolution) + "_fp_" + str(forced_prob) + "4stim/"
        if not os.path.isfile(folder + "report.pdf") and (cond.lower() == "all" or cond.lower() == "soc"):
            print("\t SOC: " + str(preruncycles) + ", " + str(cycles) + ", " + str(resolution * 2.5))
            start_time = time.time()
            soc_pop = optimisation(
                experiment=soc_exp,
                G=20,
                R=[0, 1, 0],
                q=0.01,
                weights=[0.0, 0.5, 0.25, 0.25],
                I=4,
                preruncycles=preruncycles,
                cycles=cycles,
                convergence=1000,
                folder=folder,
                outdes=1,
                Aoptimality=True,
                optimisation='GA',
                seed=loc_seed
            )
            soc_pop.optimise()
            soc_pop.download()
            print(("\t SOC: " + str(datetime.timedelta(0, time.time() - start_time)) + "\n"))


if __name__ == "__main__":
    main(sys.argv[1:])
