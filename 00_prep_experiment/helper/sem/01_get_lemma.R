### getting words for the semantic task
#install.packages("koRpus")
#install.packages("tidyverse")
#install.packages("SnowballC")
#install.packages("qdap")

library(stringr)  # for string manipulation
library(ggplot2)
library(koRpus)
library(tidyr)
library(dplyr)
library(SnowballC)
library(qdap)
rm(list=ls())

Sys.setlocale("LC_ALL", "en_US.UTF-8")
# read and crop subtlex corpus
subtlex <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/SUBTLEX-DE_cleaned_with_Google00.txt", sep = '\t', stringsAsFactors = F, encoding = "UTF-8")
subtlex <- subtlex[subtlex$spell.check.OK..1.0. == 1,]
subtlex <- subtlex[,-11]
tail(subtlex)
# # SUBTLEX: This is the frequency per million based on CUMfreqcount (i.e., it equals CUMfreqcount / 25.399). 
# This is the value to report in manuscripts, because it is a standardized value independent of the corpus size. 
# The value is given up to two decimal places in order not to lose information (notice the use of the comma as 
# the decimal sign, which is the standard in German speaking countries).

# # stemming
subtlex<- subtlex %>%
  mutate(word_stem = wordStem(subtlex$Word, language="german"))
tail(subtlex)

# get lemma
lemma_tagged <- treetag(subtlex$Word, treetagger="manual", 
                        format="obj", TT.tknz=FALSE , lang="de",
                        TT.options=list(
                          path="/home/raid2/numssen/Documents/_projects/FUNCSEG/tt", preset="de")
)
lemma_tagged_tbl <- tbl_df(lemma_tagged@TT.res)
tail(lemma_tagged_tbl)

lemmas <- subtlex %>% 
  left_join(lemma_tagged_tbl %>%
              filter(lemma != "<unknown>") %>%
              select(token, lemma, wclass, lttr),
            by = c("Word" = "token")
  )
# is.na(lemmas$lemma) has max WF of 18116
# lemmas[lemmas$WFfreqcount==18116,]
lemmas <- lemmas[!is.na(lemmas$lemma),]
lemmas <- lemmas[lemmas$wclass == "noun",]
lemmas <- lemmas[lemmas$lttr > 3,]
lemmas <- lemmas[lemmas$lttr < 15,]
lemmas$SUBTLEX <- as.numeric(sub(",",".",lemmas$SUBTLEX))
lemmas$lgSUBTLEX <- as.numeric(sub(",",".",lemmas$lgSUBTLEX))
lemmas$lgSUBTLEX <- as.numeric(sub(",",".",lemmas$lgSUBTLEX))
plot(lemmas$SUBTLEX)
ggplot(data=lemmas, aes(lgSUBTLEX))+
  geom_histogram(binwidth = .05)

ggplot(lemmas,aes(x=lttr,y=lgSUBTLEX))+
  geom_point()
rm(lemma_tagged, lemma_tagged_tbl)

# get some singular forms
toreplace = c("Bauern", "Bäume", "Freunde", "Helden", "Kerne","Knien", "Schalen", "Steine", "Urlaube")
withreplace = c("Bauer", "Baum", "Freund", "Held", "Kern","Knie", "Schale", "Stein", "Urlaub")
for (i in 1:length(toreplace)) {
  lemmas$Word <- replace(lemmas$Word, lemmas$Word == toreplace[i], withreplace[i])
}


# read LANG database
lang_all <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/Kanske-BRM-2010/all.csv", stringsAsFactors = F)
lang_fem <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/Kanske-BRM-2010/female.csv", stringsAsFactors = F)
lang_mal <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/Kanske-BRM-2010/male.csv", stringsAsFactors = F)

colnames(lang_fem) <- paste0(colnames(lang_fem),"_fem")
colnames(lang_mal) <- paste0(colnames(lang_mal),"_mal")

# lets remove some words
to_rem <- c("Bordell", "Busen", "Kondom", "Kotze", "Leichnam", "Nippel", "Penis", "Pimmel", "Pisse", "Popo", "Sperma", "Stute", "Urin")
lang_all <- cbind(lang_all, lang_fem[,-1], lang_mal[, -1])
lang_all <- lang_all[!(lang_all$word %in% to_rem),]
lang_all$aro_dif <- lang_all$arousal_mean_fem - lang_all$arousal_mean_mal
lang_all$val_dif <- lang_all$valence_mean_fem - lang_all$valence_mean_mal
lang_all$con_dif <- lang_all$concreteness_mean_fem - lang_all$concreteness_mean_mal
rm(lang_fem, lang_mal)

lang_all <- lang_all[lang_all$number_of_letters > 3,]

# 
# sum(tolower(lang_all$word) %in% tolower(lemmas$Word)) # 854
# # which are not in subtlex?
# missing_words <- data.frame(mw = as.character(lang_all$word[!(tolower(lang_all$word) %in% tolower(lemmas$Word))]))
# missing_words$mw <- as.character(missing_words$mw)
# fits <- lapply(missing_words$mw,function(x) grep(x, lemmas$Word))
# names(fits) <- missing_words$mw
# for (i in 1:length(fits)){
#   print(missing_words$mw[i])
#   print(lemmas$Word[fits[[i]]])
#   print("========================================")
# }
# lemmas$Word[charmatch(missing_words, lemmas$Word)]

rated_tlex <- merge(lemmas, lang_all, by.x = "Word", by.y = "word")

# there are no interesting word from lang_all missing
# for (word in lang_all$word) {
#   if (!(word %in% rated_tlex$Word)) {
#     print( word)
#   }
# }

# for (word in rated_tlex$Word) { # word = rated_tlex$Word[1]
#   if (sum(rated_tlex$Word == word) > 1) {
#     print(word)
#   }
# }
rated_tlex <- unique(rated_tlex)

rated_tlex <- rated_tlex[rated_tlex$SUBTLEX >=1,] # once per million or higher
rated_tlex <- rated_tlex[rated_tlex$concreteness_mean_mal <=4 & rated_tlex$concreteness_mean_fem <=4,]
ggplot(data=rated_tlex, aes(x=Word, y=concreteness_mean))+
  geom_point()+
  geom_point(aes(y=concreteness_mean_mal), color="Blue")+
  geom_point(aes(y=concreteness_mean_fem), color="Red")

mean(rated_tlex$concreteness_mean)
mean(rated_tlex$concreteness_mean_fem)
mean(rated_tlex$concreteness_mean_mal)
# only get high frequency words
# lemmas <- lemmas[lemmas$lgSUBTLEX>2.5,]
# lemmas <- lemmas[!duplicated(lemmas),]

write.table("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/words_all.csv",x =rated_tlex$Word,sep = "\t",col.names = F,row.names = F,quote = F)
# now syllafie with wuggy....

lem_syl <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/words_all_syl.csv", sep="\t",header = F,stringsAsFactors = F)  
rated_tlex <- merge(rated_tlex,lem_syl,by.x="Word",by.y="V1")
rated_tlex$nsyl <- str_count(rated_tlex$V2,"-") + 1
one_syl <- rated_tlex[rated_tlex$nsyl ==1,] # None
rated_tlex <- rated_tlex[rated_tlex$nsyl > 1 & rated_tlex$nsyl < 4,]
rated_tlex$Google00pm <- gsub(",",".",rated_tlex$Google00pm)
rated_tlex$Google00pm <- as.numeric(rated_tlex$Google00pm)

write.table("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/words.csv", x=rated_tlex, sep="\t", row.names = F,quote = F)
#rated_tlex <- rated_tlex[!(rated_tlex$word %in% to_rem),]

rm(list=ls())
## load selected words ----
df <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/words_selected_new.csv")

df <- read.csv("/home/raid2/numssen/Documents/_projects/FUNCSEG/words_rated.csv")
write.table("/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/w.csv", x=df$Word, sep=",", row.names = F,quote = F)
df <- df[df$answer>1,]
df_high <- df[df$answer<2,]

ggplot(rated_tlex, aes(SUBTLEX))+
  geom_density()
mean(df_high$SUBTLEX)
var(df_high$SUBTLEX)
write.table(df_high$Word, "/home/raid2/numssen/Documents/_projects/FUNCSEG/words_rated_concrete.csv", quote = F, row.names = F, col.names = F)

write.table(lemmas, "/home/raid2/numssen/Documents/_projects/FUNCSEG/tasks/semantics/w-pw/sub.csv", row.names=F)

a <- rated_tlex[,c("Word", "valence_mean", "valence_mean_fem", "valence_mean_mal", "arousal_mean","arousal_mean_fem","arousal_mean_mal", "concreteness_mean","concreteness_mean_fem", "concreteness_mean_mal",
                   "valence_sd", "valence_sd_fem", "valence_sd_mal", "arousal_sd","arousal_sd_fem","arousal_sd_mal", "concreteness_sd","concreteness_sd_fem", "concreteness_sd_mal")]

conc <- gather(a, group, conc, concreteness_mean,concreteness_mean_fem, concreteness_mean_mal)
arou <- gather(a, group, arou, arousal_mean,arousal_mean_fem, arousal_mean_mal)
vale <- gather(a, group, vale, valence_mean,valence_mean_fem, valence_mean_mal)


conc_sd <- gather(a, group, conc, concreteness_sd,concreteness_sd_fem, concreteness_sd_mal)
arou_sd <- gather(a, group, arou, arousal_sd,arousal_sd_fem, arousal_sd_mal)
vale_sd <- gather(a, group, vale, valence_sd,valence_sd_fem, valence_sd_mal)

conc_sd$conc <- conc_sd$conc / sqrt(32)
arou_sd$arou <- arou_sd$arou / sqrt(32)
vale_sd$vale <- vale_sd$vale / sqrt(32)

df <- data.frame(Word=conc$Word, group=conc$group, conc=conc$conc, conc_sd=conc_sd$conc, arou=arou$arou, arou_sd=arou_sd$arou, vale=vale$vale, vale_sd=vale_sd$vale)
df$group <- as.character(df$group)
df$group[df$group %in% c("concreteness_mean", "valence_mean","arousal_mean","concreteness_sd", "valence_sd","arousal_sd")] <- "all"
df$group[df$group %in% c("concreteness_mean_fem", "valence_mean_fem","arousal_mean_fem", "concreteness_sd_fem", "valence_sd_fem","arousal_sd_fem")] <- "fem"
df$group[df$group %in% c("concreteness_mean_mal", "valence_mean_mal","arousal_mean_mal", "concreteness_sd_mal", "valence_sd_mal","arousal_sd_mal")] <- "mal"


######################################
# Some stats
ggplot(data=df, aes(x=Word, y=conc, group=Word, color=group))+
  geom_point(aes())+
  geom_errorbar(data=df[df$group!="all",], aes(ymin=conc-conc_sd, ymax=conc+conc_sd))+
  geom_line(color="grey30")+
  theme(text = element_text(size=12),
         axis.text.x = element_text(angle=90, hjust=1)) 

ggplot(data=df, aes(x=Word, y=arou, group=Word, color=group))+
  geom_point(aes())+
  geom_line(color="grey30")+
  geom_errorbar(data=df[df$group!="all",], aes(ymin=arou-arou_sd, ymax=arou+arou_sd))+
  theme(text = element_text(size=12),
        axis.text.x = element_text(angle=90, hjust=1)) 

ggplot(data=df, aes(x=Word, y=vale, group=Word, color=group))+
  geom_point(aes())+
  geom_line(color="grey30")+
  geom_errorbar(data=df[df$group!="all",], aes(ymin=vale-vale_sd, ymax=vale+vale_sd))+
  theme(text = element_text(size=12),
        axis.text.x = element_text(angle=90, hjust=1)) 
