""" Calls fmriprep with singularity. """
import os
import subprocess

os.chdir('/data/pt_01994/bids')

subjects = range(1, 25)
subjects = ["{:0>2} {:0>2} {:0>2} {:0>2}".format(subjects[i],
                                                 subjects[i + 1],
                                                 subjects[i + 2],
                                                 subjects[i + 3]
                                                 ) for i in [0, 4, 8, 12, 16, 20]]
subjects[-1] = "21 22"

runs = range(5)
command = 'singularity run -B /data/pt_01994 ' \
          '/data/pt_01994/fmriprep-latest.simg ' \
          '/data/pt_01994/task_wise/bids ' \
          '/data/pt_01994/fmriprep/output/ ' \
          '--fs-license-file /data/pt_01994/fmriprep/license.txt ' \
          'participant ' \
          '--participant-label {0:0>2} ' \
          '--output-space T1w template ' \
          '--nthreads 6 ' \
          '-w /data/pt_01994/fmriprep/wd ' \
          '--fs-no-reconall ' \
          '--force-bbr'

# '--use-aroma ' \
# call 5 times in parallel
for run in runs:
    print(command.format(subjects[run]))
