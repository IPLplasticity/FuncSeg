"""Despikes all cmrr for all subjects with afni.Despike()"""
from nipype.interfaces import afni

nii_fn_base = "/data/pt_01994/probands/vp{0:0>2}/mr/ses{1}/nifti/{2}_{3}cmrr_mbep2d_bold_fast.nii"
desp_pref = 'desp_'


for vp in range(21,23):
    print(vp)
    for ses in [3]:
        print(ses)
        for run in [4,7,10,13]:
            print(run)
            # despike
            despike = afni.Despike()
            despike.inputs.in_file = nii_fn_base.format(vp, ses, run,'')
            despike.inputs.out_file = nii_fn_base.format(vp, ses, run, desp_pref)
            despike.inputs.outputtype = "NIFTI"
            despike.inputs.new25 = False
            despike.inputs.savespikes = nii_fn_base.format(vp, ses, run, 'spikes_')
            print(despike.cmdline)
            despike.run()

            # format change of spike measure to nii
            # a2n = afni.AFNItoNIFTI()
            # a2n.inputs.in_file = nii_fn_base.format(vp, ses, run, 'spikes_') + "+tlrc.HEAD"
            # a2n.inputs.out_file = nii_fn_base.format(vp, ses, run, 'spikes_')
            # # a2n.run()
            # print a2n.cmdline

            # remove afni files
            # os.remove(nii_fn_base.format(vp, ses, run, 'spikes') + "+tlrc.HEAD")
            # os.remove(nii_fn_base.format(vp, ses, run, 'spikes') + "+tlrc.BRIK")