"""Viual inspection of fmriprepped epis. Saves 1st, middle, and last slice."""
from nilearn.plotting import plot_img, plot_epi
from nilearn.image import index_img, load_img
import time
import matplotlib.pyplot as plt

fn = "/data/pt_01994/fmriprep/output/fmriprep/sub-{0:0>2}/ses-{1:0>2}/" \
     "func/sub-{0:0>2}_ses-{1:0>2}_task-alltasks_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz"

subjects = range(1, 23)
sessions = range(1, 13)

for sub in subjects:
    for ses in sessions:
        fn_sub = fn.format(sub, ses)
        print(fn_sub)
        img = load_img(fn_sub)
        for i in [0, img.shape[3] / 2, img.shape[3] - 1]:
            plot_epi(index_img(img, i),
                     title="sub-{0:0>2}_ses{1:0>2}. Slice: {2}.png".format(sub, ses, i),
                     output_file="/data/pt_01994/tmp/fmriprep_epi_plots/"
                                 "sub-{0:0>2}_ses-{1:0>2}_{2}.png".format(sub, ses, i))
    # plt.show()
    # time.sleep(1)
