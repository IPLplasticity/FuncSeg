"""Make SPM regressor file from fmriprep tsv"""
import pandas as pd

fn = "/data/pt_01994/fmriprep/output/fmriprep/sub-{0:0>2}/ses-{1:0>2}/" \
     "func/sub-{0:0>2}_ses-{1:0>2}_task-alltasks_desc-confounds_regressors.tsv"
fn_spm = "/data/pt_01994/probands/vp{0:0>2}/spm/fmriprepped/s{1:0>2}_regressors_acomp.txt"
fn_spm_info = "/data/pt_01994/probands/vp{0:0>2}/spm/fmriprepped/s{1:0>2}_regressors__acomp_info.csv"
subjects = range(1, 23)
sessions = range(1, 13)

regs = ["a_comp_cor_00", "a_comp_cor_01", "a_comp_cor_02", "a_comp_cor_03", "a_comp_cor_04", "a_comp_cor_05",
        "trans_x", "trans_y", "trans_z", "rot_x", "rot_y", "rot_z"]
fd = .9
for sub in subjects:
    print("Subject: {0:0>2}".format(sub))

    for ses in sessions:
        print("\tSession: {0:0>2}".format(ses))

        fn_sub = fn.format(sub, ses)
        fn_spm_sub = fn_spm.format(sub, ses)
        data = pd.read_csv(fn_sub, sep="\t")

        regressors = data[regs].copy()

        print("\tMax movements {0:0.3f}/{1:0.3f}/{2:0.3f} (x,y,z)".format(max(regressors["trans_x"]),
                                                                          max(regressors["trans_y"]),
                                                                          max(regressors["trans_z"])))
        print("\tFD > {0}: #{1:0>4}".format(fd, sum(data["framewise_displacement"] > fd)))
        for idx in data[data["framewise_displacement"] > fd].index:
            regressors['fd_{}'.format(idx)] = 0
            regressors['fd_{}'.format(idx)].iloc[idx] = 1

        regressors.to_csv(fn_spm_sub, sep=" ", header=False, index=False, float_format="%+1.6e")
