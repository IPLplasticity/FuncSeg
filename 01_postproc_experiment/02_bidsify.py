"""create bids like folder structure by symlinks."""

import os
import shutil
import json

basedir = ""
bidsdir = basedir + "bids/"
subjects = range(1, 23)
runs = range(1, 13)

t1_name = "t1_MPRAGE_ADNI_32"
epi_name = "{}_cmrr_mbep2d_bold_fast"
ap_fm_fn = "_cmrr_mbep2d_se_fast_norm"
pa_fm_fn = "_cmrr_mbep2d_se_fast_invPol"

# slice timing
st = [0, 265, 52.5, 317.5, 105, 372.5, 160, 425, 212.5, 0, 265, 52.5, 317.5, 105, 372.5, 160, 425, 212.5,
      0, 265, 52.5, 317.5, 105, 372.5, 160, 425, 212.5, 0, 265, 52.5, 317.5, 105, 372.5, 160, 425,
      212.5]
# convert to milliseconts
st = [t / 1000. for t in st]

# epi readout time
readout_time = 37.3999551201 / 1000.

# create json root level file
json_data = dict()
json_data['Name'] = 'causalDMN'
json_data['BIDSVersion'] = '1.2.0-dev'
root_json_fn = '{}dataset_description.json'.format(bidsdir)
with open(root_json_fn, 'w') as f:
    json.dump(json_data, f)

# create json root level file: task
json_data = dict()
json_data['TaskName'] = 'alltasks'
root_json_fn = '{}task-alltasks_bold.json'.format(bidsdir)
with open(root_json_fn, 'w') as f:
    json.dump(json_data, f)

for sub in subjects:

    try:
        # t1_bids_name = "sub{:0>2}_T1w.nii"
        subdir_bids = "{}sub-{:0>2}/".format(bidsdir, sub)
        os.mkdir(subdir_bids)

        subdir_org = "{}probands/vp{:0>2}/".format(basedir, sub)

        anat_folder = "{}anat/".format(subdir_bids)
        os.mkdir(anat_folder)

        t1_fn = "{}probands/vp{:0>2}/mr/t1/{}.nii".format(basedir, sub, t1_name)
        t1_bids_fn = "{}sub-{:0>2}_T1w.nii".format(anat_folder, sub)
        assert os.path.exists(t1_fn), "{} does not exist".format(t1_fn)
        os.symlink(t1_fn, t1_bids_fn)

        t1_fn = "{}probands/vp{:0>2}/mr/t1/{}.json".format(basedir, sub, t1_name)
        t1_bids_fn = "{}sub-{:0>2}_T1w.json".format(anat_folder, sub)
        assert os.path.exists(t1_fn), "{} does not exist".format(t1_fn)
        os.symlink(t1_fn, t1_bids_fn)

        for run in runs:
            sesdir = "{}ses-{:0>2}/".format(subdir_bids, run)
            os.mkdir(sesdir)

            func_folder = "{}func/".format(sesdir)
            os.mkdir(func_folder)

            if run in (1, 2, 3, 4):
                ses = 1
            elif run in (5, 6, 7, 8):
                ses = 2
            elif run in (9, 10, 11, 12):
                ses = 3
            else:
                raise ValueError

            if run in (1, 5, 9):
                epi_pref = 4
            elif run in (2, 6, 10):
                epi_pref = 7
            elif run in (3, 7, 11):
                epi_pref = 10
            elif run in (4, 8, 12):
                epi_pref = 13
            else:
                raise ValueError

            run_epi_name_org = epi_name.format(epi_pref)
            run_epi_name_org_fn = "{}mr/ses{}/nifti/{}.{}".format(subdir_org, ses, run_epi_name_org, 'nii')
            assert os.path.exists(run_epi_name_org_fn), "{} does not exist".format(run_epi_name_org_fn)
            run_epi_name_bids = "{}sub-{:0>2}_ses-{:0>2}_task-alltasks_bold.nii".format(func_folder, sub, run)
            os.symlink(run_epi_name_org_fn, run_epi_name_bids)

            # epi_data = dict()
            # epi_data['RepetitionTime'] = .5
            # epi_data['TaskName'] = 'alltasks'
            # epi_data['SliceTiming'] = st
            # epi_data_json = "{}sub-{:0>2}_ses-{:0>2}_task-alltasks_bold.json".format(func_folder, sub, run)
            #
            # with open(epi_data_json, 'w') as f:
            #     json.dump(epi_data, f)
            run_epi_name_org_fn = "{}mr/ses{}/nifti/{}.{}".format(subdir_org, ses, run_epi_name_org, 'json')
            run_epi_name_bids = "{}sub-{:0>2}_ses-{:0>2}_task-alltasks_bold.json".format(func_folder, sub, run)
            assert os.path.exists(run_epi_name_org_fn), "{} does not exist".format(run_epi_name_org_fn)
            os.symlink(run_epi_name_org_fn, run_epi_name_bids)

            # fmap
            fmap_folder = "{}fmap/".format(sesdir)
            os.makedirs(fmap_folder)

            # AP
            sub_fm_ap_org = "{}mr/ses{}/nifti/{}{}.{}".format(subdir_org, ses, epi_pref - 2, ap_fm_fn, 'nii')
            sub_fm_ap_bids = "{}sub-{:0>2}_ses-{:0>2}_dir-AP_epi.nii".format(fmap_folder, sub, run)
            assert os.path.exists(sub_fm_ap_org), "{} does not exist".format(sub_fm_ap_org)
            os.symlink(sub_fm_ap_org, sub_fm_ap_bids)

            sub_fm_ap_json_org = "{}mr/ses{}/nifti/{}{}.{}".format(subdir_org, ses, epi_pref - 2, ap_fm_fn, 'json')
            sub_fm_ap_bids = "{}sub-{:0>2}_ses-{:0>2}_dir-AP_epi.json".format(fmap_folder, sub, run)

            with open(sub_fm_ap_json_org, 'r') as f:
                json_org = json.load(f)
            json_ap = dict()
            json_ap['PhaseEncodingDirection'] = json_org['PhaseEncodingDirection']
            json_ap['IntendedFor'] = "ses-{:0>2}/func/sub-{:0>2}_ses-{:0>2}_task-alltasks_bold.nii".format(run, sub,
                                                                                                           run)
            json_ap['TotalReadoutTime'] = readout_time

            with open(sub_fm_ap_bids, 'w') as f:
                json.dump(json_ap, f)

            sub_fm_ap_bids = "{}sub-{:0>2}_ses-{:0>2}_dir-AP_epi.json".format(fmap_folder, sub, run)

            # PA
            sub_fm_pa_org = "{}mr/ses{}/nifti/{}{}.{}".format(subdir_org, ses, epi_pref - 1, pa_fm_fn, 'nii')
            sub_fm_pa_bids = "{}sub-{:0>2}_ses-{:0>2}_dir-PA_epi.nii".format(fmap_folder, sub, run)
            assert os.path.exists(sub_fm_pa_org), "{} does not exist".format(sub_fm_pa_org)
            os.symlink(sub_fm_pa_org, sub_fm_pa_bids)

            sub_fm_pa_json_org = "{}mr/ses{}/nifti/{}{}.{}".format(subdir_org, ses, epi_pref - 1, pa_fm_fn, 'json')
            sub_fm_pa_bids = "{}sub-{:0>2}_ses-{:0>2}_dir-PA_epi.json".format(fmap_folder, sub, run)
            with open(sub_fm_pa_json_org, 'r') as f:
                json_org = json.load(f)
            json_ap = dict()
            json_ap['PhaseEncodingDirection'] = json_org['PhaseEncodingDirection']
            json_ap['IntendedFor'] = "ses-{:0>2}/func/sub-{:0>2}_ses-{:0>2}_task-alltasks_bold.nii".format(run, sub,
                                                                                                           run)
            json_ap['TotalReadoutTime'] = readout_time

            with open(sub_fm_pa_bids, 'w') as f:
                json.dump(json_ap, f)

    except Exception as e:
        print(e)
