"""fileformats.py: read and process data / tags from dicom files, convert between nifti and dicom."""
import warnings

warnings.filterwarnings("ignore", category=UserWarning)

import nibabel.nicom.dicomwrappers
from copy import deepcopy
# from ..helper.util import *
import tarfile
import shutil
import glob
import os

__author__ = "Ole Numssen"
__status__ = "Development"
__email__ = "numssen@cbs.mpg.de"


def get_dicom_dir(basedir, vp, session):
    dicom_dir = os.path.join(basedir, vp, "mr/ses{}".format(session), "dicom",
                             os.listdir(os.path.join(basedir, vp, "mr/ses{}".format(session), "dicom"))[0])
    return os.path.join(dicom_dir, "DICOM", "")


def get_nifti_dir(basedir, vp, session):
    return os.path.join(basedir, vp, "mr/ses{}".format(session), "nifti")


def create_topup_data_in(fn, total_readout_time, n_img=2):
    """https://lcni.uoregon.edu/kb-articles/kb-0003:
    "Create a "datain" file. This is a text file with four columns. The first three columns indicate the phase encode
    direction, and the fourth is the total readout time (defined as the time from the center of the first echo to the
    center of the last) in seconds for the se-epi acquisition."

    The distortion scan here (@cbs|mpi) have two volumes per direction.

    Example output:
    0 -1 0 .0351
    0 -1 0 .0351
    0 1 0 .0351
    0 1 0 .0351

    Example Call:
    create_topup_data_in("/data/pt_01994/tmp/test.txt",
        calc_total_readout_time("/data/pt_01994/probands/vp02/mr/ses1/dicom/
                                 15484.08_20180806_092000.PRISMA/DICOM/00020001"))

    :param fn: Full file path where file will be saved
    :type fn: basestring
    :param total_readout_time: totalReadoutTime [s]
    :type total_readout_time: float
    :param n_img: How many images per direction? cbs standard seems to be 2. If you average them -> n_img=1
    :type n_img: int
    :return: fn
    :rtype: file
    """

    with open(fn, 'w') as f:
        for _ in range(n_img):
            f.write("0 -1 0 {0:3f}\n".format(total_readout_time))
        for _ in range(n_img):
            f.write("0  1 0 {0:3f}\n".format(total_readout_time))


def calc_total_readout_time(fn, n_img=2, version="FSL"):
    """https://lcni.uoregon.edu/kb-articles/kb-0003:
    In the case of a simple EPI sequence, with no parallel imaging, no partial fourier, no phase oversampling, etc,
    this is straightforward enough. For the general case, it's appropriate to consider this an "effective" readout time,
     calculated using the effective echo spacing and the number of reconstructed phase lines:

    :param fn: Full filepath to one of the 4 AP/PA DICOM scans
    :type fn: basestring
    :param version: FSL or SPM
    :type version: basestring
    :return: TotalReadoutTime (FSL / SPM)
    :rtype: float
    """

    if version is "FSL":
        return (get_matrix_size_phase(fn) - 1) * calc_effective_echo_spacing(fn)
    elif version is "SPM":
        return 1. / get_bandwith_per_pixel_phase_encode(fn)
    else:
        raise NotImplementedError


def check_readout_times(ap_dcm_files):
    """Checks if readout time for each file in ap_dcm_files is the same

    :param ap_dcm_files: list of filenames
    :type ap_dcm_files: list<basestring>
    :return: True if all the same, False if not
    :rtype: bool
    """
    res = calc_total_readout_time(ap_dcm_files[0])
    for f in ap_dcm_files[1:]:
        if not res == calc_total_readout_time(f):
            return False
    return True


def calc_effective_echo_spacing(fn):
    """https://lcni.uoregon.edu/kb-articles/kb-0003:
    Multiplying [Bandwidth Per Pixel Phase Encode"] by the size of the reconstructed image in the phase direction (aka
    the number of reconstructed phase lines) will give you the reciprocal of the effective echo spacing
    :param fn:
    :type fn:
    :return: Effective Echo Spacing [s]
    :rtype: float
    """
    return 1. / (get_bandwith_per_pixel_phase_encode(fn) * get_matrix_size_phase(fn))


# we need this for the field map calculation
def get_bandwith_per_pixel_phase_encode(fn):
    """https://lcni.uoregon.edu/kb-articles/kb-0003:
    We use the "effective" echo spacing, rather than the actual echo spacing, in order to include the effects of
    parallel imaging, phase oversampling, etc. Siemens has helpfully included this information in the DICOM file with
    the parameter "Bandwidth Per Pixel Phase Encode", stored in DICOM tag (0019, 1028).

    :param fn: Full filepath to one of the 4 AP/PA DICOM scans
    :type fn: basestring
    :return: BandwidthPerPixelPhaseEncode
    :rtype: int
    """

    img = nibabel.nicom.dicomwrappers.wrapper_from_file(fn)
    return img.dcm_data[0x0019, 0x1028].value


def get_matrix_size_phase(fn):
    """https://lcni.uoregon.edu/kb-articles/kb-0003:
    The size of the image in the phase direction is *usually* the first number in the field (0051, 100b),
    AcquisitionMatrixText. This is, however, not always the case, so it is better to determine the actual size
    (rows or columns) of your reconstructed image in the phase encode direction. You can also just read the effective
    echo spacing from the Series Info screen in MRIConvert if you are running the latest version.

    :param fn: Full filepath to one of the 4 AP/PA DICOM scans
    :type fn: basestring
    :return: MatrixSizePhase
    :rtype: int"""

    img = nibabel.nicom.dicomwrappers.wrapper_from_file(fn)
    assert int(img.dcm_data[0x0051, 0x100b].value[0:2]) == img.dcm_data[0x0018, 0x1310][0]

    return int(img.dcm_data[0x0051, 0x100b].value[0:2])


def dcm2nifti(probands=None, sessions=None, convertt1=False, cleandir=True, t1_fn=None):
    basedir = "/data/pt_01994/probands"
    t1_fn_cor = "t1_MPRAGE_ADNI_32"

    if not type(sessions) == list:
        sessions = [sessions]
    assert all([type(item) == int for item in sessions])

    if not type(probands) == list:
        probands = [probands]
    assert all([isinstance(proband, str) for proband in probands])

    if sessions is None:
        sessions = [1, 2, 3]
    if not probands:
        probands = os.listdir(basedir)
        probands.sort()
    for proband in probands:
        if proband.startswith("vp") and os.listdir(os.path.join(basedir, proband)) and proband != "vpX":
            print("Converting proband {}".format(proband))
            for session in sessions:
                try:
                    print("    Session {}".format(session))
                    dicomdir = os.walk(os.path.join(basedir, proband, "mr", "ses{}".format(session), "dicom")).next()[1]
                    niftidir = os.path.join(basedir, proband, "mr", "ses{}".format(session), "nifti")
                    dicomdir = os.path.join(basedir, proband, "mr", "ses{}".format(session), "dicom",
                                            dicomdir[0], "DICOM")
                    if not os.path.isdir(niftidir):
                        os.mkdir(niftidir)
                    else:
                        if cleandir:
                            shutil.rmtree(niftidir)
                            os.mkdir(niftidir)

                    command = "dcm2niix -o {} -f %s_%p {}".format(niftidir, dicomdir)
                    os.system(command)

                    if convertt1:
                        if cleandir:
                            filetoremove = glob.glob(os.path.join(basedir,
                                                                  proband,
                                                                  "mr",
                                                                  "t1") + "/*nii")
                            for f in filetoremove:
                                print("Removing " + f)
                                os.remove(f)

                            filetoremove = glob.glob(os.path.join(basedir,
                                                                  proband,
                                                                  "mr",
                                                                  "t1") + "/*json")
                            for f in filetoremove:
                                print("Removing old file: " + f)
                                os.remove(f)
                        # convert t1 dicom to nifti
                        tar_fn = glob.glob(os.path.join(basedir,
                                                        proband,
                                                        "mr",
                                                        "t1", "*tar.gz"))[0]

                        t1tar = tarfile.open(tar_fn)
                        t1tar.extractall(path=os.path.split(tar_fn)[0])
                        tar_fn = tar_fn[:-7]
                        command = "dcm2niix -o {} -f %p {}".format(os.path.join(basedir,
                                                                                proband,
                                                                                "mr",
                                                                                "t1", ""), tar_fn)
                        os.system(command)
                        # rename t1 if necessary
                        if t1_fn and t1_fn != t1_fn_cor:
                            shutil.move(os.path.join(basedir, proband, "mr", "t1", t1_fn + ".nii"),
                                        os.path.join(basedir, proband, "mr", "t1", t1_fn_cor + ".nii"))
                    print("================================================================================")

                except Exception:
                    print("Something went wrong.")
        else:
            print("Proband {} not known.".format(proband))


def create_symlinks(vp, force=False):
    """This creates symlinks from the niftis to FOLDER/spm"""
    basedir = os.path.join(vp.folder, 'spm')
    targets = [os.path.join(basedir, str(run) + "_cmrr_mbep2d_bold_fast.nii") for run in range(1, 13)[::-1]]
    sources = deepcopy(vp.epi_fn_full)
    if not any([os.path.exists(link) for link in targets]) or force:
        for link in targets:
            if force:
                try:
                    os.unlink(link)
                except OSError:
                    pass
            try:
                print("creating symlink from {}{}".format(sources[-1], link))
                os.symlink(sources.pop(), link)

            except OSError as e:
                "OS error({0}): {1}".format(e.errno, e.strerror)
