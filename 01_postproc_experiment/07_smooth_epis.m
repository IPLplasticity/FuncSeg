function smooth_fmriprepped(vps)
vps = get_vp_names(vps);
n_cpu = 11;
parfor(vpi =1:size(vps,1), n_cpu)
% for vpi =1:size(vps,1)
    vp = vps(vpi,:);
    batch_fn = [['/data/pt_01994/probands/' vp '/spm/fmriprepped/s8w_sub-' vp(3:4) '_ses-1_to_12.m'];]
    matlabbatch = get_batch(vp);
    write_batch_mfile(matlabbatch, batch_fn, {}, '8mm smoothing of fmriprepped bold data');
    spm('defaults', 'FMRI');
    spm_jobman('run', matlabbatch);
end
%%
end


function matlabbatch = get_batch(vp)
for run=1:12
    matlabbatch{run}.spm.spatial.smooth.data = get_scans(vp,run);
    matlabbatch{run}.spm.spatial.smooth.fwhm = [8 8 8];
    matlabbatch{run}.spm.spatial.smooth.dtype = 0;
    matlabbatch{run}.spm.spatial.smooth.im = 0;
    matlabbatch{run}.spm.spatial.smooth.prefix = 's8';
end
end


function scans = get_scans(vp,run)

fn = ['/data/pt_01994/probands/' vp '/spm/fmriprepped/w_sub-' vp(3:4) '_ses-' sprintf('%02d',run) '_fmriprepped_func.nii'];
run_nifti = nifti(fn);
run_vols = run_nifti.dat.dim(4);

% cellstr([repmat([fn ','], run_vols,1) num2str([1:run_vols]', '%-i')]);
scans = cellstr([repmat([fn ','], run_vols,1) num2str([1:run_vols]', '%-i')]);
end