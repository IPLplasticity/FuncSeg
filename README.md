# Hemispheric specialization of the inferior parietal lobe across cognitive domains

This repository holds code used in our paper _Hemispheric specialization of 
the inferior parietal lobe across cognitive domains (Numssen, Bzdok, Hartwigsen, in prep.)_.    

## Content
* 00_prep_experiment  
  Preperation of the fMRI experiment, i.e. generation of the subject-wise designs.

* 01_postproc_experiment  
  Postprocessing steps of the raw fMRI data.  

* 02_stats

* 03_plotting
